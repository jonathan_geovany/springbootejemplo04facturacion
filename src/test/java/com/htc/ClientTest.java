package com.htc;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.exception.AbstractException;
import com.htc.model.ClientEntity;
import com.htc.service.ClientService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientTest {
	private static final Logger log = LoggerFactory.getLogger(ClientTest.class);
	
	@Autowired ClientService clientService;
	
	@Test
	public void clientTest() throws AbstractException {
		
		log.info("Mostrando los clientes");
		List<ClientEntity> list = clientService.getAll();
		for (ClientEntity clientEntity : list) {
			log.info(clientEntity.toString());
		}
		ClientEntity c = new ClientEntity("Registro nuevo","31231-123-12","012311-1","7734-1231","Sonsonate");
		//assertEquals(true, clientService.insert(c));
		
		clientService.insert(c);
		
		c.setNames("modificado 123");
		log.info("actualizando");
		clientService.update(c);
		
		log.info("Lista de clientes actualizada");
		list = clientService.getAll();
		for (ClientEntity clientEntity : list) {
			log.info(clientEntity.toString());
		}
		
		log.info("eliminando");
		clientService.delete(c);
		
		log.info("Lista de clientes actualizada");
		list = clientService.getAll();
		for (ClientEntity clientEntity : list) {
			log.info(clientEntity.toString());
		}
		
		
	}
}
