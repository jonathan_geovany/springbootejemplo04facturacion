package com.htc;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTests {
	
	
	
	
	//se ejecuta una sola vez y se llama antes de realizar los test
	@BeforeClass
    public static void beforeClass() {
    	
    }
	
	//se ejecuta antes de realizar cada test
	@Before
    public void setUpTest() {
        
    }
	
	@Test
	public void testCliente() {
		
	}
	
	@Test
	public void testCondicionPago() {
	    
	}
	
	//despues de cada prueba
	@After
	public void tearDownTest() {
		
	}
	
	//se ejecuta antes de destruir la clase 
	@AfterClass
	public static void tearDownClass() {
		
	}

}

