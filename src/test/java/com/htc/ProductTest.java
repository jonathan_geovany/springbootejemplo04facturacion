package com.htc;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.exception.AbstractException;
import com.htc.model.ProductEntity;
import com.htc.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTest {
	private static final Logger log = LoggerFactory.getLogger(ProductTest.class);
	
	@Autowired ProductService productService;
	
	@Test
	public void productTest() throws AbstractException {
		
		List<ProductEntity> list = productService.getAll();
		for (ProductEntity productEntity : list) {
			log.info(productEntity.toString());
		}
		ProductEntity p = new ProductEntity("prod05", "Laptop IBM E47", 20, new BigDecimal(1100.00));
		//productService.insert(p);
		p = productService.getItemById(5);
		p.setStock(100);
		productService.update(p);
		
		
	}
}
