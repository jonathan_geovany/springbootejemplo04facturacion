package com.htc;



import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.exception.AbstractException;
import com.htc.model.BusinessEntity;
import com.htc.service.BusinessService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessTest {
	private static final Logger log = LoggerFactory.getLogger(BusinessTest.class);
	
	@Autowired BusinessService businessService;
	
	@Test
	public void businessTest() throws AbstractException {
		
		BusinessEntity b = businessService.getItemById(6);
		b.setBusinessName("Nombre negocio");
		b.setTradename("tradename");
		b.setCity("ciudad");
		businessService.delete(b);
		
		log.info("mostrar lista de negocios");
		List<BusinessEntity> list = businessService.getAll();
		for (BusinessEntity businessEntity : list) {
			log.info(businessEntity.toString());
		}
		
		//BusinessEntity b = new BusinessEntity("High Tech Consulting", "HTC SA de CV", "desarrollo", "123102-123-12", "213-1", "El Salvador", "San Salvador", "Col. Santa Elena, San Salvador", "2424-2323", "7272-6262", "info@htc.com", new BigDecimal(0.13));
		//insercion
		//businessService.insert(b);
		//b.setBusinessName("HTC Desarollo");
		//b.setId(4);
		//assertEquals(true, businessService.update(b));
		
	}
}
