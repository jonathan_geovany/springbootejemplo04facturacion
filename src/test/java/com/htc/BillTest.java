package com.htc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.exception.AbstractException;
import com.htc.model.BillEntity;
import com.htc.model.BillingDetailEntity;
import com.htc.service.BillService;
import com.htc.service.BillingDetailsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillTest {

	private static final Logger log = LoggerFactory.getLogger(BillTest.class);
	
	@Autowired BillService billService;
	
	@Autowired BillingDetailsService billingDetailsService;
	
	@Test
	public void billTest() throws AbstractException {
		//utilizando todos los metodos
		/*List<BillingDetails> detalles = new ArrayList<>();
		
		java.util.Date utilDate = new java.util.Date();
		java.sql.Timestamp sq = new java.sql.Timestamp(utilDate.getTime());
		
		detalles.add(  billingDetailsService.getBillingDetails(
				1,
				20)
				);
		detalles.add( billingDetailsService.getBillingDetails(
				"prod02",
				20)
				);
		Bill factura =  new Bill(1,1,1,1,1,"Paga victor","A002",sq,detalles);
		billService.insert(factura);
		
		*/
		log.info("Mostrando todas las facturas");
		List<BillEntity> list=null;
		try {
			list = billService.getAllBills();
		} catch (AbstractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (BillEntity billEntity : list) {
			log.info(billEntity.toString());
		}
		log.info("Mostrando las facturas no nulas");
		try {
			list = billService.getNonNullBills();
		} catch (AbstractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (BillEntity billEntity : list) {
			log.info(billEntity.toString());
		}
		
		//fecha
		java.util.Date utilDate = new java.util.Date();
		java.sql.Timestamp sq = new java.sql.Timestamp(utilDate.getTime());
		//agregando detalles
		
		List<BillingDetailEntity> detalles = new ArrayList<>();
		detalles.add( billingDetailsService.getBillingDetails(3, 10));
		detalles.add( billingDetailsService.getBillingDetails(4, 15));
		detalles.add( billingDetailsService.getBillingDetails(5, 20));
		
		BillEntity billEntity=new BillEntity(1,1,1,1,1,"prueba 29/01/2019","A001",sq,detalles);
		
		billEntity = billService.getBillById(21);
		
		billService.setNullBill(billEntity);
		
		//billService.insert(billEntity);
		System.out.println("Mostrando datos");
		List<String[]> result=null;
		try {
			result = billService.getQuery("select f.id, f.register, empr.tradename , empl.names , clie.names, c_pago.description , f.subtotal, f.tax , f.taxAmount,f.total from bill f \r\n" + 
					"	inner join business  empr	on f.idBusiness 	= empr.id\r\n" + 
					"	inner join employee empl	on f.idEmployee 	= empl.id\r\n" + 
					"	inner join client  clie		on f.idClient 	= clie.id\r\n" + 
					"	inner join payment pay		on	f.idPayment	= pay.id\r\n" + 
					"	inner join billStatus c_pago on f.idStatus = c_pago.id;");
		} catch (AbstractException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (String[] strings : result) {
			System.out.println(Arrays.toString(strings));
		}
		
		//System.out.println("ID "+bill.getId());
		
		//bill.setObservations("observacion nueva");
		
		//log.info(billService.getBillById(1).toString());
		
		//billService.update(bill);
		//billService.setNullBill(bill);
		
		
	}
}
