package com.htc;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.exception.AbstractException;
import com.htc.model.BillingDetailEntity;
import com.htc.service.BillingDetailsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillDetailsTest {
		
	private static final Logger log = LoggerFactory.getLogger(BillDetailsTest.class);
	
	@Autowired  BillingDetailsService billingDetailsService;
	@Test
	public void billDetailsTest() throws AbstractException {
		//probar cada metodo
		
		log.info("Mostrando detalles de facturas");
		List<BillingDetailEntity> list = billingDetailsService.getAll();
		for (BillingDetailEntity billingDetailEntity : list) {
			log.info(billingDetailEntity.toString());
		}
		
		Integer idBill = 1;
		log.info("Mostrando detalles en factura "+idBill);
		list = billingDetailsService.getAllInBill(idBill);
		for (BillingDetailEntity billingDetailEntity : list) {
			log.info(billingDetailEntity.toString());
		}
		Integer idBillDetail = 1;
		log.info("Mostrando detalle de factura con id "+idBillDetail);
		BillingDetailEntity billingDetailEntity = billingDetailsService.getItemById(idBillDetail);
		log.info(billingDetailEntity.toString());
		
		log.info("Cantidad de detalles de factura "+billingDetailsService.getCount());
		idBillDetail = 2;
		log.info("Obteniendo detalles en base al id de producto "+idBillDetail);
		BillingDetailEntity detalle1 = billingDetailsService.getBillingDetails(idBillDetail, 2);
		log.info(detalle1.toString());
		
		String code = "prod01";
		log.info("Obteniendo detalles en base al codigo de producto");
		detalle1 = billingDetailsService.getBillingDetails(code, 2);
		log.info(detalle1.toString());
		
		
	}

}
