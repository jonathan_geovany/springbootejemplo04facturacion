package com.htc;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.exception.AbstractException;
import com.htc.model.PaymentEntity;
import com.htc.service.PaymentService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentTest {
	
	private static final Logger log = LoggerFactory.getLogger(PaymentTest.class);
	
	
	@Autowired PaymentService paymentService;
	
	@Test
	public void paymentTest() throws AbstractException {

		log.info("pruebas iniciadas");
		
		PaymentEntity paymentEntity = new PaymentEntity("nuevo");
		
		paymentService.insert(paymentEntity);
		
		paymentEntity.setDescription("desc");
		
		paymentService.update(paymentEntity);
		
		paymentService.delete(paymentEntity);
		
		
	}
}
