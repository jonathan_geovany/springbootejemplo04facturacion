package com.htc;


import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.exception.AbstractException;
import com.htc.model.EmployeeEntity;
import com.htc.service.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeTest {
	
	private static final Logger log = LoggerFactory.getLogger(EmployeeTest.class);
	
	
	@Autowired EmployeeService employeeService;
	
	@Test
	public void employeeTest() throws AbstractException {
		log.info("Mostrando lista de empleados");
		List<EmployeeEntity> list = employeeService.getAll();
		for (EmployeeEntity employeeEntity : list) {
			log.info(employeeEntity.toString());
		}
		
		EmployeeEntity e = new EmployeeEntity(1,"Empleado 2","123343-1","programador",new BigDecimal(350.0),"7723-1233","Sonsonate","nuevo@trabajo.com");
		employeeService.insert(e);
		
		e.setNames("Employee 2");
		employeeService.update(e);
		
		employeeService.delete(e);
		
		
	}
}
