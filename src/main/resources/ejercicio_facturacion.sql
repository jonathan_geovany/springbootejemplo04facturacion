-- Database: ejercicio_spring

--DROP DATABASE ejercicio_spring;

CREATE DATABASE ejercicio_spring
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

create table business(
	id				serial 			primary key,
	tradename		text			not null,
	businessName	text			not null,
	sector			varchar(80)		,
	nit				varchar(20)		not null,
	nrc				varchar(20)		not null,
	country			varchar(80)		,
	city			varchar(80)		,
	address			text			,
	phone1			varchar(20)		,
	phone2			varchar(20)		,
	email			varchar(80)		,
	tax				numeric(4,2)	not null
);

create table employee(
	id				serial		primary key,
	idBusiness		integer		references 	business(id) 	not null,
	names			text		not null,
	dui				varchar(20)	not null,
	position		varchar(20)	,
	salary			numeric(6,2),
	phone			varchar(20)	,
	address			text		,
	email			varchar(80)	
);

create table client(
	id			serial			primary key,
	names		text			not null,
	nit			varchar(20)		,
	dui			varchar(20)		,
	phone		varchar(20)		,
	address		text			
);


create table payment(
	id			serial			primary key,
	description	text			not null
);

create table billStatus(
	id			serial			primary key,
	description	text			not null
);

create table bill(
	id 			serial 			primary key,
	idBusiness	integer			references 	business(id) 	not null,
	idEmployee 	integer		 	references 	employee(id) 	not null,
	idClient	integer			references	client(id)		not null,
	idPayment 	integer			references	payment(id) 	not null,
	idStatus 	integer			references	billStatus(id) 	not null,
	observations	text		,
	register	varchar(50) 	not null,
	emitted		timestamp 		not null,
	tax			numeric(10,2)	not null,
	taxAmount	numeric(10,2)	not null,
	subtotal	numeric(10,2)	not null,
	total		numeric(10,2)	not null
);

create table product(
	id			serial primary key,
	code		varchar(80)		unique not null,
	description	text			not null,
	stock		integer 		not null,
	price		numeric(10,2)	not null
);

create table billingDetails(
	id			serial 			primary key,
	idBill	integer 		references bill(id),
	idProduct	integer			references product(id),
	price		numeric(10,2)	not null,
	quantity	integer			not null,
	total		numeric(10,2)	not null
);

-- inserciones de prueba
SELECT *
FROM information_schema.columns
WHERE table_name = 'bill';

insert into business(tradename,businessName,sector,nit,nrc,country,city,address,phone1,phone2,email,tax) values 
	('Electromundo','Asociacion Electromundo S.A. de C.V.','ventas','0614-290290-001-0','215-1','El Salvador','San Salvador','Col. Escalon 2a Av Norte #2-4','(+503) 2429 0505','(+503) 7751 2121','contacto@electromundo.com',0.13);

select * from business;

insert into employee(idBusiness,names,dui,position,salary,phone,address,email) values 
	(1,'Juan Luis Perez Sosa','05080508-0','cajero',500.00,'(+503) 7744 0101','San Salvador Col. Santa Elena 4a Calle Poniente #32','juan_perez@electromundo.com'),
	(1,'Ana Maria Martinez Flores','07129108-0','cajero',500.00,'(+503) 7041 9921','San Salvador Col. El Espino 3a Av. Norte #3-1','ana_martinez@electromundo.com');

select * from employee;
	
insert into client(names,nit,dui,phone,address) values 
	('cliente','','','',''),
	('Francisco Javier Castillo Torres','0614-290209-001-0','0706050-4','(+503) 7745 5466','San Salvador Col. El Angel 2a Calle poniente #3-1');

select * from client;

insert into payment(description) values
	('Contado'),
	('Debito');

select * from payment;


insert into billStatus(description) values
	('Emitida'),
	('Nula');

select * from billStatus;

insert into bill(idBusiness,idEmployee,idClient,idPayment,idStatus,observations,register,emitted,tax,taxAmount,subtotal,total) values 
	(1,1,1,1,1,'procesada normalmente','A001','2018-12-29 16:43:21',0.13,3.71,28.5,32.21),
	(1,2,2,1,1,'procesada normalmente','A001','2019-01-15 10:22:04',0.13,4.09,31.49,35.58);
	
select * from bill;

select f.id, f.register, empr.tradename , empl.names , clie.names, c_pago.description , f.subtotal, f.tax , f.taxAmount,f.total from bill f 
	inner join business  empr	on f.idBusiness 	= empr.id
	inner join employee empl	on f.idEmployee 	= empl.id
	inner join client  clie		on f.idClient 	= clie.id
	inner join payment pay		on	f.idPayment	= pay.id
	inner join billStatus c_pago on f.idStatus = c_pago.id;

insert into product(code,description,stock,price) values
	('prod01','Mouse inalambrico 2.4 Ghz',20,5.0),
	('prod02','Memoria USB 16 GB 2.0',30,3.50),
	('prod03','Cable Carga USB a micro usb 1.8 Mt',30,2.99),
	('prod04','Camara web logitech C270',10,25.00);

select * from product;

insert into billingDetails(idBill,idProduct,price,quantity,total) values
	(1,2,3.5,1,3.5),
	(1,3,25.0,1,25.0),
	(2,2,3.5,1,3.5),
	(2,3,2.99,1,2.99),
	(2,4,25.00,1,25.00);
	
select * from billingDetails;






		





