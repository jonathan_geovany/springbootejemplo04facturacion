package com.htc.mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.ProductEntity;

public class ProductRowMapper implements RowMapper<ProductEntity> {

	@Override
	public ProductEntity mapRow(ResultSet arg0, int arg1) throws SQLException {
		ProductEntity p = new ProductEntity();
		
		p.setId((Integer)			arg0.getObject(1));
		p.setCode((String)			arg0.getObject(2));
		p.setDescription((String)	arg0.getObject(3));
		p.setStock((Integer)		arg0.getObject(4));
		p.setPrice((BigDecimal)			arg0.getObject(5));
		
		return p;
	}

}
