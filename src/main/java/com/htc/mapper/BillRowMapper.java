package com.htc.mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.BillEntity;

public class BillRowMapper implements RowMapper<BillEntity>{

	@Override
	public BillEntity mapRow(ResultSet arg0, int arg1) throws SQLException {
		
		BillEntity f = new BillEntity();
		
		f.setId((Integer)				arg0.getObject(1));
		f.setIdBusiness((Integer)		arg0.getObject(2));
		f.setIdEmployee((Integer)		arg0.getObject(3));
		f.setIdClient((Integer)			arg0.getObject(4));
		f.setIdPayment((Integer)		arg0.getObject(5));
		f.setIdStatus((Integer)			arg0.getObject(6));
		f.setObservations((String)		arg0.getObject(7));
		f.setRegister((String)			arg0.getObject(8));
		f.setEmitted((Timestamp)		arg0.getObject(9));
		f.setTax((BigDecimal)				arg0.getObject(10));
		f.setTaxAmount((BigDecimal)			arg0.getObject(11));
		f.setSubtotal((BigDecimal)			arg0.getObject(12));
		f.setTotal((BigDecimal)				arg0.getObject(13));
		
		return f;
	}

}
