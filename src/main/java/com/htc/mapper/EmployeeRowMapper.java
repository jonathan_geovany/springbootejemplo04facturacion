package com.htc.mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.EmployeeEntity;

public class EmployeeRowMapper implements RowMapper<EmployeeEntity> {

	@Override
	public EmployeeEntity mapRow(ResultSet arg0, int arg1) throws SQLException {
		
		EmployeeEntity e = new EmployeeEntity();
		e.setId((Integer)			arg0.getObject(1));
		e.setIdBusiness((Integer)	arg0.getObject(2));
		e.setNames((String)			arg0.getObject(3));
		e.setDui((String)			arg0.getObject(4));
		e.setPosition((String)		arg0.getObject(5));
		e.setSalary((BigDecimal)			arg0.getObject(6));
		e.setPhone((String)			arg0.getObject(7));
		e.setAddress((String)		arg0.getObject(8));
		e.setEmail((String)			arg0.getObject(9));
		return e;
	}

}
