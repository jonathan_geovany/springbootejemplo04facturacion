package com.htc.mapper;



import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.ClientEntity;


public class ClientRowMapper implements RowMapper<ClientEntity> {

	@Override
	public ClientEntity mapRow(ResultSet arg0, int arg1) throws SQLException {
		
		ClientEntity c = new ClientEntity();
		c.setId((Integer)			arg0.getObject(1));
		c.setNames((String)			arg0.getObject(2));
		c.setNit((String)			arg0.getObject(3));
		c.setDui((String)			arg0.getObject(4));
		c.setPhone((String)			arg0.getObject(5));
		c.setAddress((String) 		arg0.getObject(6));
		
		return c;
	}

}
