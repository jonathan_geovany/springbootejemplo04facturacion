package com.htc.mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.BusinessEntity;

public class BusinessRowMapper implements RowMapper<BusinessEntity>{

	@Override
	public BusinessEntity mapRow(ResultSet arg0, int arg1) throws SQLException {
		
		BusinessEntity e = new BusinessEntity();
		e.setId((Integer)				arg0.getObject(1));
		e.setTradename((String)			arg0.getObject(2));
		e.setBusinessName((String)		arg0.getObject(3));
		e.setSector((String)			arg0.getObject(4));
		e.setNit((String)				arg0.getObject(5));
		e.setNrc((String)				arg0.getObject(6));
		e.setCountry((String)			arg0.getObject(7));
		e.setCity((String)				arg0.getObject(8));
		e.setAddress((String)			arg0.getObject(9));
		e.setPhone1((String)			arg0.getObject(10));
		e.setPhone2((String)			arg0.getObject(11));
		e.setEmail((String)				arg0.getObject(12));
		e.setTax((BigDecimal)				arg0.getObject(13));
		return e;
	}

}
