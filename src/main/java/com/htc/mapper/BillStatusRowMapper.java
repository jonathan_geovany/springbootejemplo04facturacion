package com.htc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.BillStatusEntity;

public class BillStatusRowMapper implements RowMapper<BillStatusEntity> {

	@Override
	public BillStatusEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		BillStatusEntity c = new BillStatusEntity();
		c.setId((Integer)			rs.getObject(1));
		c.setDescription((String)	rs.getObject(2));
		
		return c;
	}

}
