package com.htc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.PaymentEntity;

public class PaymentRowMapper implements RowMapper<PaymentEntity>{

	@Override
	public PaymentEntity mapRow(ResultSet arg0, int arg1) throws SQLException {
		
		PaymentEntity c = new PaymentEntity();
		c.setId((Integer)			arg0.getObject(1));
		c.setDescription((String)	arg0.getObject(2));
		
		return c;
	}

}
