package com.htc.mapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.htc.model.BillingDetailEntity;

public class BillingDetailsRowMapper implements RowMapper<BillingDetailEntity> {

	@Override
	public BillingDetailEntity mapRow(ResultSet arg0, int arg1) throws SQLException {
		
		BillingDetailEntity d = new BillingDetailEntity();
		d.setId((Integer)			arg0.getObject(1));
		d.setIdBill((Integer)		arg0.getObject(2));
		d.setIdProduct((Integer)	arg0.getObject(3));
		d.setPrice((BigDecimal) 			arg0.getObject(4));
		d.setQuantity((Integer)		arg0.getObject(5));
		d.setTotal((BigDecimal)			arg0.getObject(6));
		
		return d;
	}

}
