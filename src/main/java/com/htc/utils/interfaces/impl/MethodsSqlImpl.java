package com.htc.utils.interfaces.impl;

import org.springframework.stereotype.Component;

import com.htc.utils.interfaces.MethodsSql;

@Component
public class MethodsSqlImpl implements MethodsSql {
	
	private static final String WHERE = " WHERE ";

	@Override
	public String select(String table) {
		return "SELECT * FROM " + table;
	}
	
	@Override
	public String selectById(String table,String id) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(select(table));
		stringBuilder.append(WHERE);
		stringBuilder.append(id);
		stringBuilder.append("=?");
		return stringBuilder.toString();
	}
	
	@Override
	public String insert(String table,String values,boolean autoIncrement) {
		StringBuilder builder = new StringBuilder();
		builder.append("INSERT INTO ");
		builder.append(table);
		
		int columns = values.split(",").length+1;
		
		if(autoIncrement) {
			builder.append("(");
			builder.append(values);
			builder.append(")");
			columns--;
		}
		builder.append(" VALUES (");
				
		for(int i=0;i<columns;i++) {
			if(i<columns-1) {
				builder.append("?,");
			} else {
				builder.append("?");
			}
		}
		builder.append(")");
		return builder.toString();
	}

	@Override
	public String delete(String table, String id) {
		return "DELETE FROM "+table+WHERE+id+"=?";
	}

	@Override
	public String update(String table,String values, String id) {
		StringBuilder builder = new StringBuilder();
		builder.append("UPDATE "+table+" SET ");
		String[] columns = values.split(",");
		for(int i=0;i<columns.length;i++) {
			builder.append(columns[i]);
			if(i<columns.length-1) {
				builder.append("=?,");
			} else {
				builder.append("=?");
			}
		}
		builder.append(WHERE);
		builder.append(id);
		builder.append("=?");
		return builder.toString();
	}

	@Override
	public String count(String table) {
		return "SELECT count(*) FROM "+table;
	}

	@Override
	public String msg(Exception e) {
		StringBuilder builder = new StringBuilder();
		builder.append(e.getCause());
		builder.append(" Error: ");
		builder.append(e.getMessage());
		return builder.toString();
	}

	@Override
	public String msg(Exception e, StackTraceElement stack) {
		StringBuilder builder = new StringBuilder();
		builder.append(msg(e));
		builder.append(" class : ");
		builder.append(stack.getClassName());
		builder.append(" method: ");
		builder.append(stack.getMethodName());
		builder.append(" line: ");
		builder.append(stack.getLineNumber());
		return builder.toString();
	}
	
	
}
