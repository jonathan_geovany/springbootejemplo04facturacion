package com.htc.utils.interfaces;

public interface MethodsSql {
	public String select(String table);
	public String selectById(String table,String id);
	public String insert(String table,String values,boolean autoIncrement);
	public String delete(String table,String id);
	public String update(String table,String values,String id);
	public String count(String table);
	public String msg(Exception e);
	public String msg(Exception e,StackTraceElement stack);
}
