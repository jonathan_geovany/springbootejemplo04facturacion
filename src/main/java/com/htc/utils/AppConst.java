package com.htc.utils;

public final class AppConst {

	private AppConst() {
		throw new IllegalStateException("AppConst class");
	}

	// para tabla cliente
	public static final String TABLE_CLIENT = "client";
	public static final String ID_CLIENT = "id";
	public static final String VALUES_CLIENT = "names,nit,dui,phone,address";

	// para tabla tipo de pago
	public static final String TABLE_PAYMENT = "payment";
	public static final String ID_PAYMENT = "id";
	public static final String VALUES_PAYMENT = "description";

	// para tabla estado de factura
	public static final String TABLE_BILL_STATUS = "billStatus";
	public static final String ID_BILL_STATUS = "id";
	public static final String VALUES_BILL_STATUS = "description";

	// para tabla detalle factura
	public static final String TABLE_BILLING_DETAILS = "billingDetails";
	public static final String ID_BILLING_DETAILS = "id";
	public static final String VALUES_BILLING_DETAILS = "idBill,idProduct,price,quantity,total";
	public static final String FILTER_BILLING_DETAILS = "SELECT * FROM " + TABLE_BILLING_DETAILS + " WHERE idBill=?";
	public static final String CUSTOM_BILL_QUERY = "select f.id, f.register, empr.tradename , empl.names , clie.names, c_pago.description , f.subtotal, f.tax , f.taxAmount,f.total from bill f \r\n"
			+ "	inner join business  empr	on f.idBusiness 	= empr.id\r\n"
			+ "	inner join employee empl	on f.idEmployee 	= empl.id\r\n"
			+ "	inner join client  clie		on f.idClient 	= clie.id\r\n"
			+ "	inner join payment pay		on	f.idPayment	= pay.id\r\n"
			+ "	inner join billStatus c_pago on f.idStatus = c_pago.id;";
	// para la tabla empleado
	public static final String TABLE_EMPLOYEE = "employee";
	public static final String ID_EMPLOYEE = "id";
	public static final String VALUES_EMPLOYEE = "idBusiness,names,dui,position,salary,phone,address,email";

	// para la tabla empresa
	public static final String TABLE_BUSINESS = "business";
	public static final String ID_BUSINESS = "id";
	public static final String VALUES_BUSINESS = "tradename,businessName,sector,nit,nrc,country,city,address,phone1,phone2,email,tax";

	// para la tabla factura
	public static final String TABLE_BILL = "bill";
	public static final String ID_BILL = "id";
	public static final String VALUES_BILL = "idBusiness,idEmployee,idClient,idPayment,idStatus,observations,register,emitted,tax,taxAmount,subtotal,total";
	public static final String VALUES_BILL_UPDATE = "observations";
	public static final String VALUES_BILL_NULL = "idStatus,observations";
	public static final String NON_NULL_BILLS = "select * from " + TABLE_BILL + " where idStatus!=2";

	// para la tabla producto
	public static final String TABLE_PRODUCT = "product";
	public static final String ID_PRODUCT = "id";
	public static final String VALUES_PRODUCT = "code,description,stock,price";
	public static final String UPDATE_PRODUCT = "update "+TABLE_PRODUCT+" set stock=? where id=?";
	
	
	/** codigos y mensajes de ok y falla por defecto */
	public static final Long DEFAULT_OK_CODE = 200L;
	public static final Long DEFAULT_FAIL_INPUT_CODE = 400L;
	public static final Long DEFAULT_FAIL_SERVER_CODE = 500L;
	
	public static final String OK_INSERT = "Registro guardado exitosamente";
	public static final String OK_UPDATE = "Registro actualizado exitosamente";
	public static final String OK_DELETE = "Registro eliminado exitosamente";
	public static final String OK_QUERY = "Registro consultado exitosamente";
	public static final String OK_NULL = "Registro anulado exitosamente";

	public static final String FAIL_INSERT = "No se pudo guardar el registro";
	public static final String FAIL_UPDATE = "No se pudo actualizar el registro";
	public static final String FAIL_DELETE = "No se pudo eliminar el registro";
	public static final String FAIL_QUERY = "No se pudo realizar la consulta al registro";
	public static final String FAIL_NULL = "No se pudo anular el registro";

	/** codigos y mensajes de error **/
	
	// errores de entrada del usuario 400 a 499
	public static final Long CODE_NULL_OBJECT= 400L;
	public static final String MSG_NULL_OBJECT = "No se puede procesar dato nulo ";
	
	public static final Long CODE_NULL_EMPTY = 401L;
	public static final String MSG_NULL_EMPTY = "Falta introducir valores: ";

	public static final Long CODE_SIGNED_VALUE = 402L;
	public static final String MSG_SIGNED_VALUE = "Los siguientes parametros no deben llevar numeros negativos: ";

	public static final Long CODE_NON_ZERO = 403L;
	public static final String MSG_NON_ZERO = "Los siguientes parametros deben ser mayor a cero: ";

	public static final Long CODE_STOCK = 404L;
	public static final String MSG_STOCK = "Cantidad de venta sobrepasa stock de producto: ";

	public static final Long CODE_NOT_FOUND_BILL = 405L;
	public static final String MSG_NOT_FOUND_BILL = "No se encontro factura con el id: ";

	public static final Long CODE_NOT_FOUND_DETAILS = 406L;
	public static final String MSG_NOT_FOUND_DETAILS = "No se encontro detalle con el id: ";

	public static final Long CODE_NOT_FOUND_STATUS = 407L;
	public static final String MSG_NOT_FOUND_STATUS = "No se encontro el estado de factura con el id: ";

	public static final Long CODE_NOT_FOUND_BUSINESS = 408L;
	public static final String MSG_NOT_FOUND_BUSINESS = "No se encontro el empresa con el id: ";

	public static final Long CODE_NOT_FOUND_CLIENT = 409L;
	public static final String MSG_NOT_FOUND_CLIENT = "No se encontro el cliente con el id: ";

	public static final Long CODE_NOT_FOUND_EMPLOYEE = 410L;
	public static final String MSG_NOT_FOUND_EMPLOYEE = "No se encontro el empleado con el id: ";

	public static final Long CODE_NOT_FOUND_PAYMENT = 411L;
	public static final String MSG_NOT_FOUND_PAYMENT = "No se encontro el tipo de pago con el id: ";

	public static final Long CODE_NOT_FOUND_PRODUCT = 412L;
	public static final String MSG_NOT_FOUND_PRODUCT = "No se encontro el producto con el id: ";

	public static final Long CODE_CANT_NULL_BILL = 413L;
	public static final String MSG_CANT_NULL_BILL = "Factura ya se encuentra anulada";

	
	
	// errores de capa service 500 a 599
	public static final Long CODE_DB_CONN = 501L;
	public static final String MSG_DB_CONN = "No se pudo conectar con la base de datos";

	public static final Long CODE_NULL_DATA = 502L;
	public static final String MSG_NULL_DATA = "Dato nulo no se puede continuar el proceso";

	public static final Long CODE_NO_UPDATED_ROWS = 503L;
	public static final String MSG_NO_UPDATED_ROWS = "no hay filas actualizadas en: ";

	public static final Long CODE_CANT_INSERTED = 504L;
	public static final String MSG_CANT_INSERTED = "no se pudo insertar en: ";

}
