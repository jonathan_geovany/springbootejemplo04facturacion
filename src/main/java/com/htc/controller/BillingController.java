package com.htc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.htc.billing.Bill;
import com.htc.billing.GetAllBillsResponse;
import com.htc.billing.GetBillByIdResponse;
import com.htc.billing.InsertBillResponse;
import com.htc.billing.SetNullBillResponse;
import com.htc.expose.BillingExposing;

@RestController
@RequestMapping("/billing")
public class BillingController {
	
	private static final String MESSAGE = "Message";
	@Autowired
	private BillingExposing billingExposing;
	
	@PostMapping("/")
	ResponseEntity<String> newEmployee(@RequestBody Bill b) {
		InsertBillResponse response = billingExposing.insertBill(b);
		HttpHeaders headers = new HttpHeaders();
	    headers.add(MESSAGE, response.getResult().getMsg());
		return new ResponseEntity<>(null,headers,HttpStatus.valueOf( (int) response.getResult().getCode()));
	}
	
	@GetMapping(path = "/", produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<Bill>> getAllBills() {
		GetAllBillsResponse response = billingExposing.getAllBills();
		HttpHeaders headers = new HttpHeaders();
	    headers.add(MESSAGE, response.getResult().getMsg());
		return new ResponseEntity<>(response.getBills(),headers,HttpStatus.valueOf( (int) response.getResult().getCode()));
	}
	
	@GetMapping(path = "/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Bill> getBillById(@PathVariable int id) {
		GetBillByIdResponse response = billingExposing.getBillById(id);
		HttpHeaders headers = new HttpHeaders();
	    headers.add(MESSAGE, response.getResult().getMsg());
		return new ResponseEntity<>(response.getBill(),headers,HttpStatus.valueOf( (int) response.getResult().getCode()));
	}
	
	@DeleteMapping(path = "/", produces=MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<String> cancelBill(@RequestParam int id,@RequestParam String observation) {
		SetNullBillResponse response = billingExposing.setNullBill(id, observation);
		HttpHeaders headers = new HttpHeaders();
	    headers.add(MESSAGE, response.getResult().getMsg());
		return new ResponseEntity<>(null,headers,HttpStatus.valueOf( (int) response.getResult().getCode()));
	}
	
	
}
