package com.htc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.billing.GetAllProductsResponse;
import com.htc.expose.BillingExposing;

@RestController
@RequestMapping("/products")
public class ProductController {
	

	@Autowired
	private BillingExposing billingExposing;
	
	@GetMapping(path = "/", produces=MediaType.APPLICATION_JSON_VALUE)
	GetAllProductsResponse getAllProducts() {
		return billingExposing.getAllProducts();
	}

}
