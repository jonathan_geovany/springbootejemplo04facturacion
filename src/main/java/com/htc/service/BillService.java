package com.htc.service;

import java.util.List;

import com.htc.billing.Bill;
import com.htc.exception.AbstractException;
import com.htc.model.BillEntity;

public interface BillService {
	public void insert(BillEntity c) throws AbstractException;
	public boolean update(BillEntity c) throws AbstractException;
	public void insertList(List<BillEntity> list) throws AbstractException;
	public boolean setNullBill(BillEntity c) throws AbstractException;
	public List<BillEntity> getAllBills() throws AbstractException;
	public List<BillEntity> getNonNullBills() throws AbstractException;
	public List<String[]> getQuery(String sql) throws AbstractException;
	public BillEntity getBillById(Integer id) throws AbstractException;
	public int getCount() throws AbstractException;
	
	public Bill toDto(BillEntity mBill);
	public List<Bill> toDtoList(List<BillEntity> mList);
	public BillEntity toEntity(Bill rBill)throws AbstractException;
	
}
