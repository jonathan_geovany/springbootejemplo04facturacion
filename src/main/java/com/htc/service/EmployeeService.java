package com.htc.service;

import java.util.List;

import com.htc.exception.AbstractException;
import com.htc.model.EmployeeEntity;

public interface EmployeeService {
	public void insert(EmployeeEntity c) throws AbstractException;
	public boolean update(EmployeeEntity c) throws AbstractException;
	public int[] insertList(List<EmployeeEntity> list) throws AbstractException;
	public boolean delete(EmployeeEntity c) throws AbstractException;
	public List<EmployeeEntity> getAll() throws AbstractException;
	public EmployeeEntity getItemById(Integer id) throws AbstractException;
	public int getCount() throws AbstractException;
}
