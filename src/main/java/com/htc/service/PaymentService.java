package com.htc.service;

import java.util.List;

import com.htc.exception.AbstractException;
import com.htc.model.PaymentEntity;

public interface PaymentService {
	public void insert(PaymentEntity c) throws AbstractException;
	public boolean update(PaymentEntity c) throws AbstractException;
	public int[] insertList(List<PaymentEntity> list) throws AbstractException;
	public boolean delete(PaymentEntity c) throws AbstractException;
	public List<PaymentEntity> getAll() throws AbstractException;
	public PaymentEntity getItemById(Integer id) throws AbstractException;
	public int getCount() throws AbstractException;
}
