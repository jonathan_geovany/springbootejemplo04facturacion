package com.htc.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.billing.Product;
import com.htc.dao.ProductDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.ProductEntity;
import com.htc.service.ProductService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Component
public class ProductServiceImpl implements ProductService{
	
	private static final String STOCK_PRICE = " Stock, price";

	private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
	
	@Autowired
	private MethodsSql methodsSql;
	@Autowired
	private ProductDao productDao;
	
	@Override
	public void insert(ProductEntity c) throws AbstractException{
		try {
			String eval = checkProductValues(c, false);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			if(c.getPrice().signum()<0 || c.getStock()<0) throw new InputRequestException(AppConst.CODE_SIGNED_VALUE,AppConst.MSG_SIGNED_VALUE+STOCK_PRICE);
			Integer idProduct  = productDao.insertAndReturnKey(c);
			if(idProduct==null)  throw new ServiceException(AppConst.CODE_CANT_INSERTED,AppConst.MSG_CANT_INSERTED+ProductEntity.class.getSimpleName());
			c.setId(idProduct);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	
	@Override
	public int[] insertList(List<ProductEntity> list)throws AbstractException {
		try {
			for (ProductEntity c : list) {
				String eval = checkProductValues(c, true);
				if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
				if(c.getPrice().signum()<0 || c.getStock()<0) throw new InputRequestException(AppConst.CODE_SIGNED_VALUE,AppConst.MSG_SIGNED_VALUE+STOCK_PRICE);
			}
			return productDao.insertList(list);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean delete(ProductEntity c) throws AbstractException{
		try {
			if(c==null || c.getId()==null)throw  new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			return productDao.delete(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<ProductEntity> getAll()throws AbstractException {
		try {
			return productDao.getAll();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public ProductEntity getItemById(Integer id)throws AbstractException {
		try {
			return productDao.getItemById(id);
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int getCount() throws AbstractException{
		try {
			return productDao.getCount();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public ProductEntity getItemByCode(String code)throws AbstractException {
		try {
			return productDao.getItemByCode(code);
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
		
	}
	
	@Override
	public boolean update(ProductEntity c) throws AbstractException{
		try {
			String eval = checkProductValues(c, true);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			if(c.getPrice().signum()<0 || c.getStock()<0) throw new InputRequestException(AppConst.CODE_SIGNED_VALUE,AppConst.MSG_SIGNED_VALUE+STOCK_PRICE);
			return productDao.update(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}
	
	private String checkProductValues(ProductEntity f,boolean isUpdate) {
		StringBuilder builder = new StringBuilder();
		if(f==null) return ProductEntity.class.getSimpleName();
		if(isUpdate && f.getId()==null) builder.append("id ");
		if(f.getCode()==null || f.getCode().isEmpty()) builder.append("code ");
		if(f.getDescription()==null || f.getDescription().isEmpty()) builder.append("description ");
		if(f.getPrice()==null) builder.append("price ");
		if(f.getStock()==null) builder.append("stock ");
		return builder.toString();
	}


	@Override
	public Product toDto(ProductEntity mProduct) {
		Product rProduct = new Product();
		rProduct.setId(mProduct.getId());
		rProduct.setCode(mProduct.getCode());
		rProduct.setDescription(mProduct.getDescription());
		rProduct.setStock(mProduct.getStock());
		rProduct.setPrice(mProduct.getPrice());
		return rProduct;
	}


	@Override
	public List<Product> toDtoList(List<ProductEntity> mProducts) {
		List<Product> rProducts = new ArrayList<>();
		for (ProductEntity mP : mProducts) {
			rProducts.add(toDto(mP));
		}
		return rProducts;
	}
}
