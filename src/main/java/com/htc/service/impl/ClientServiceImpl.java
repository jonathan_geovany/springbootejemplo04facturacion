package com.htc.service.impl;

import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.htc.dao.ClientDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.ClientEntity;
import com.htc.service.ClientService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Service
public class ClientServiceImpl implements ClientService {

	private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);


	@Autowired 
	private MethodsSql methodsSql;
	@Autowired 
	private ClientDao clientDao;
	
	@Override
	public void insert(ClientEntity c)throws AbstractException {
		try {
			String eval = checkClientValues(c, false);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			Integer idClient  = clientDao.insertAndReturnKey(c);
			if(idClient==null) throw new ServiceException(AppConst.CODE_CANT_INSERTED,AppConst.MSG_CANT_INSERTED+ClientEntity.class.getSimpleName());
			c.setId(idClient);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int[] insertList(List<ClientEntity> list) throws AbstractException{
		try {
			for (ClientEntity c : list) {
				String eval = checkClientValues(c, true);
				if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			}
			return clientDao.insertList(list);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean delete(ClientEntity c)throws AbstractException {
		try {
			if(c==null||c.getId()==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			
			return clientDao.delete(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<ClientEntity> getAll()throws AbstractException {
		try {
			return clientDao.getAll();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public ClientEntity getItemById(Integer id)throws AbstractException {
		try {
			if(id==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			return clientDao.getItemById(id);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int getCount() throws AbstractException{
		try {
			return clientDao.getCount();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean update(ClientEntity c) throws AbstractException{
		try {
			String eval = checkClientValues(c, true);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			return clientDao.update(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}
	
	private String checkClientValues(ClientEntity f,boolean isUpdate) {
		StringBuilder builder = new StringBuilder();
		if(f==null) return ClientEntity.class.getSimpleName();
		if(isUpdate && f.getId()==null) builder.append("id ");
		if(f.getNames()==null || f.getNames().isEmpty()) builder.append("names ");
		return builder.toString();
	}
}
