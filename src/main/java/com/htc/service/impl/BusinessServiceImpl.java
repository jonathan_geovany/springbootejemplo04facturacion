 package com.htc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.dao.BusinessDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.BusinessEntity;
import com.htc.service.BusinessService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Service
public class BusinessServiceImpl implements BusinessService {
	
	private static final Logger log = LoggerFactory.getLogger(BusinessServiceImpl.class);
	
	@Autowired 
	private MethodsSql methodsSql;
	@Autowired 
	private BusinessDao businessDao;
	
	@Override
	public void insert(BusinessEntity c) throws AbstractException{
		try {
			String eval = checkBusinessValues(c, false);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			
			Integer idBusiness = businessDao.insertAndReturnKey(c);
			if(idBusiness==null) throw new ServiceException(AppConst.CODE_CANT_INSERTED,AppConst.MSG_CANT_INSERTED+BusinessEntity.class.getSimpleName());
			c.setId(idBusiness);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int[] insertList(List<BusinessEntity> list) throws AbstractException{
		try {
			for (BusinessEntity c : list) {
				String eval = checkBusinessValues(c, false);
				if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			}
			return businessDao.insertList(list);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean delete(BusinessEntity c) throws AbstractException {
		try {
			if(c==null || c.getId()==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			return businessDao.delete(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<BusinessEntity> getAll() throws AbstractException{
		try {
			return businessDao.getAll();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public BusinessEntity getItemById(Integer id) throws AbstractException{
		try {
			if(id==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			return businessDao.getItemById(id);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int getCount()throws AbstractException {
		try {
			return businessDao.getCount();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean update(BusinessEntity c)throws AbstractException {
		try {
			String eval = checkBusinessValues(c, true);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			return businessDao.update(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}
	
	
	private String checkBusinessValues(BusinessEntity f,boolean isUpdate) {
		StringBuilder builder = new StringBuilder();
		if(f==null) return BusinessEntity.class.getSimpleName();
		if(isUpdate && f.getId()==null) builder.append("id ");
		if(f.getBusinessName()==null || f.getBusinessName().isEmpty()) builder.append("Business name ");
		if(f.getNit()==null || f.getNit().isEmpty()) builder.append("NIT ");
		if(f.getNrc()==null || f.getNrc().isEmpty()) builder.append("NRC ");
		if(f.getTax()==null ) builder.append("Tax ");
		
		return builder.toString();
	}

}
