package com.htc.service.impl;

import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.dao.EmployeeDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.EmployeeEntity;
import com.htc.service.EmployeeService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private static final Logger log = LoggerFactory.getLogger(EmployeeServiceImpl.class);
	
	@Autowired
	private MethodsSql methodsSql;
	@Autowired
	private EmployeeDao employeeDao;
	
	@Override
	public void insert(EmployeeEntity c)throws AbstractException {
		try {
			String eval = checkEmployeeValues(c, false);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			Integer idEmployee  = employeeDao.insertAndReturnKey(c);
			if(idEmployee==null) throw new ServiceException(AppConst.CODE_CANT_INSERTED,AppConst.MSG_CANT_INSERTED+EmployeeEntity.class.getSimpleName());
			c.setId(idEmployee);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int[] insertList(List<EmployeeEntity> list) throws AbstractException{
		try {
			for (EmployeeEntity c : list) {
				String eval = checkEmployeeValues(c, true);
				if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			}
			return employeeDao.insertList(list);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean delete(EmployeeEntity c)throws AbstractException {
		try {
			if(c==null||c.getId()==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			return employeeDao.delete(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<EmployeeEntity> getAll() throws AbstractException{
		try {
			return employeeDao.getAll();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public EmployeeEntity getItemById(Integer id) throws AbstractException{
		try {
			if(id==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			return employeeDao.getItemById(id);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int getCount()throws AbstractException {
		try {
			return employeeDao.getCount();
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean update(EmployeeEntity c)throws AbstractException {
		try {
			String eval = checkEmployeeValues(c, true);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			return employeeDao.update(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}
	
	private String checkEmployeeValues(EmployeeEntity f,boolean isUpdate) {
		StringBuilder builder = new StringBuilder();
		if(f==null) return EmployeeEntity.class.getSimpleName();
		if(isUpdate && f.getId()==null) builder.append("id ");
		if(f.getNames()==null || f.getNames().isEmpty()) builder.append("names ");
		if(f.getDui()==null || f.getDui().isEmpty()) builder.append("DUI ");
		return builder.toString();
	}
}
