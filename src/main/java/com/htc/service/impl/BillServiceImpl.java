package com.htc.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.htc.billing.Bill;
import com.htc.dao.BillDao;
import com.htc.dao.BillingDetailsDao;
import com.htc.dao.BusinessDao;
import com.htc.dao.ClientDao;
import com.htc.dao.EmployeeDao;
import com.htc.dao.PaymentDao;
import com.htc.dao.ProductDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.BillEntity;
import com.htc.model.BillingDetailEntity;
import com.htc.model.BusinessEntity;
import com.htc.model.ProductEntity;
import com.htc.service.BillService;
import com.htc.service.BillingDetailsService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;


@Service
public class BillServiceImpl implements BillService {

	private static final Logger log = LoggerFactory.getLogger(BillServiceImpl.class);
	
	@Autowired 
	private BillDao billDao;
	@Autowired 
	private BusinessDao businessDao;
	@Autowired 
	private BillingDetailsDao billingDetailsDao;
	@Autowired
	private BillingDetailsService billingDetailsService;
	@Autowired 
	private ProductDao productDao;	
	@Autowired 
	private ClientDao clientDao;
	@Autowired 
	private EmployeeDao employeeDao;	
	@Autowired 
	private PaymentDao paymentDao;
	@Autowired 
	private MethodsSql methodsSql;
	
	
	@Transactional
	@Override
	public void insert(BillEntity f) throws AbstractException {
		try {
			//evaluar campos de la factura
			String eval = nullOrEmptyBillValues(f);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			
			//establezco el id de la factura como emitida
			f.setIdStatus(1);
			//evaluar cliente, tipo de pago y empleado
			if(employeeDao.getItemById(f.getIdEmployee())==null) throw new InputRequestException(AppConst.CODE_NOT_FOUND_EMPLOYEE,AppConst.MSG_NOT_FOUND_EMPLOYEE+f.getIdEmployee());
			if(clientDao.getItemById(f.getIdClient())==null) throw new InputRequestException( AppConst.CODE_NOT_FOUND_CLIENT,AppConst.MSG_NOT_FOUND_CLIENT+f.getIdClient());
			if(paymentDao.getItemById(f.getIdPayment())==null) throw new InputRequestException(AppConst.CODE_NOT_FOUND_PAYMENT,AppConst.MSG_NOT_FOUND_PAYMENT+f.getIdPayment());
			
			//obtener valor de iva de la empresa
			BusinessEntity businessEntity = businessDao.getItemById(f.getIdBusiness());
			if(businessEntity==null) throw new InputRequestException( AppConst.CODE_NOT_FOUND_BUSINESS,AppConst.MSG_NOT_FOUND_BUSINESS+f.getIdBusiness() );
			f.setTax(businessEntity.getTax());
			
			//calculos sobre los detalles
			BigDecimal subtotal  = new BigDecimal(0);
			List<ProductEntity> productEntities = new ArrayList<>();
			for (BillingDetailEntity d : f.getDetails()) {
				//validaciones
				if(d==null || d.getProduct()==null ) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY+"Detalle o Producto");
				if(d.getQuantity()>d.getProduct().getStock()) throw new InputRequestException(AppConst.CODE_STOCK,AppConst.MSG_STOCK+d.getProduct().getCode());
				
				//calculos
				d.getProduct().setStock( d.getProduct().getStock() - d.getQuantity() );
				d.setTotal( d.getPrice().multiply(new BigDecimal(d.getQuantity())) );
				subtotal = subtotal.add(d.getTotal());
				//guardar lista de productos con stock modificado
				productEntities.add(d.getProduct());
			}
			//si no se pudo actualizar la lista de productos lanza exception
			if(!productDao.updateList(productEntities)) throw new ServiceException(AppConst.CODE_NO_UPDATED_ROWS,AppConst.MSG_NO_UPDATED_ROWS+ProductEntity.class.getSimpleName());
		
			f.setSubtotal(subtotal);
			f.setTaxAmount(f.getTax().multiply(f.getSubtotal()));
			f.setTotal( f.getSubtotal().add(f.getTaxAmount()));
			
			//agregar fecha del sistema
			java.util.Date utilDate = new java.util.Date();
			java.sql.Timestamp emitted = new java.sql.Timestamp(utilDate.getTime());
			f.setEmitted(emitted);
			
			//validad el id de la factura
			Integer idBill = billDao.insertAndReturnKey(f);
			if(idBill==null) throw new ServiceException(AppConst.CODE_CANT_INSERTED,AppConst.MSG_CANT_INSERTED+BillEntity.class.getSimpleName());
			f.setId(idBill);
			if(billingDetailsDao.insertList(f.getDetails(),idBill)==null) throw new ServiceException(AppConst.CODE_CANT_INSERTED,AppConst.MSG_CANT_INSERTED+BillingDetailEntity.class.getSimpleName());
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Transactional
	@Override
	public void insertList(List<BillEntity> list) throws AbstractException{
		try {
			for (BillEntity billEntity : list) {
				insert(billEntity);
			}
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Transactional
	@Override
	public boolean setNullBill(BillEntity c) throws AbstractException {
		try {
			String eval = checkValuesForUpdateOrAnnul(c);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			//establecer id de estado a nulo
			
			BillEntity bill = getBillById(c.getId());
			if(bill==null) throw new ServiceException(AppConst.CODE_NOT_FOUND_BILL,AppConst.MSG_NOT_FOUND_BILL);
			if(bill.getIdStatus()==2) throw new InputRequestException(AppConst.CODE_CANT_NULL_BILL, AppConst.MSG_CANT_NULL_BILL);
			c.setIdStatus(2);
			List<BillingDetailEntity> details =  bill.getDetails();
			List<ProductEntity> products = new ArrayList<>();
			for (BillingDetailEntity b : details) {
				ProductEntity p =productDao.getItemById(b.getIdProduct());
				p.setId(b.getIdProduct());
				p.setStock( p.getStock() + b.getQuantity());
				products.add(p);
			}
			if(!productDao.updateList(products)) throw new ServiceException(AppConst.CODE_NO_UPDATED_ROWS,AppConst.MSG_NO_UPDATED_ROWS);
			return billDao.setNullBill(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<BillEntity> getAllBills() throws AbstractException {
		try {
			List<BillEntity> list = billDao.getAllBills();
			for (BillEntity billEntity : list) {
				billEntity.setDetails( billingDetailsDao.getAllInBill(billEntity.getId()) );
			}
			return list;
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public BillEntity getBillById(Integer id) throws AbstractException{
		try {
			if(id==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			BillEntity billEntity = billDao.getBillById(id);
			if(billEntity==null) throw new InputRequestException(AppConst.CODE_NOT_FOUND_BILL,AppConst.MSG_NOT_FOUND_BILL+ id);
			billEntity.setDetails(billingDetailsDao.getAllInBill(id));
			return billEntity;
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int getCount() throws AbstractException{
		try {
			return billDao.getCount();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean update(BillEntity c) throws AbstractException{
		try {
			String eval = checkValuesForUpdateOrAnnul(c);
			if(!eval.isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+eval);
			return billDao.update(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<BillEntity> getNonNullBills() throws AbstractException{
		try {
			List<BillEntity> list = billDao.getNonNullBills();
			for (BillEntity billEntity : list) {
				billEntity.setDetails( billingDetailsDao.getAllInBill(billEntity.getId()) );
			}
			return list;
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<String[]> getQuery(String sql) throws AbstractException {
		try {
			List<Map<String, Object>> result = billDao.getQuery(sql);
			List<String[]> params = new ArrayList<>();
			for (Map<String, Object> map : result) {
				params.add( Arrays.toString(map.values().toArray()).replaceAll("[\\[\\]]", "").split(","));
			}
			return params;
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}
	

	private String checkValuesForUpdateOrAnnul(BillEntity c) {
		StringBuilder builder = new StringBuilder();
		if(c==null) return BillEntity.class.getSimpleName();
		if(c.getId()==null) builder.append("id ");
		if(c.getObservations()==null || c.getObservations().isEmpty()) builder.append("observation ");
		return builder.toString();
	}
	
	private String nullOrEmptyBillValues(BillEntity f) {
		StringBuilder builder = new StringBuilder();
		if(f==null) return BillEntity.class.getSimpleName();
		if(f.getIdBusiness()==null) builder.append("idBusiness ");
		if(f.getIdEmployee()==null) builder.append("idEmployee ");
		if(f.getIdPayment()==null) builder.append("idPayment ");
		if(f.getIdClient()==null) builder.append("idClient ");
		if(f.getRegister()==null || f.getRegister().isEmpty() || f.getRegister().length()<2) builder.append("register ");
		if(f.getDetails()==null || f.getDetails().isEmpty()) builder.append("details ");
		return builder.toString();
	}

	@Override
	public Bill toDto(BillEntity mBill){
		try {
			if(mBill==null) throw new InputRequestException(AppConst.CODE_NULL_OBJECT,AppConst.MSG_NULL_OBJECT);
			Bill rBill = new Bill();
			rBill.setId(mBill.getId());
			rBill.setIdBusiness(mBill.getIdBusiness());
			rBill.setIdEmployee(mBill.getIdEmployee());
			rBill.setIdClient(mBill.getIdClient());
			rBill.setIdPayment(mBill.getIdPayment());
			rBill.setIdStatus(mBill.getIdStatus());
			rBill.setObservations(mBill.getObservations());
			rBill.setRegister(mBill.getRegister());
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTimeInMillis(mBill.getEmitted().getTime());
			XMLGregorianCalendar gc =
			DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
			rBill.setEmitted(gc);
			rBill.setTax(mBill.getTax());
			rBill.setTaxAmount(mBill.getTaxAmount());
			rBill.setSubtotal(mBill.getSubtotal());
			rBill.setTotal(mBill.getTotal());
			rBill.getBillingDetails().addAll( billingDetailsService.toDtoList(mBill.getDetails()));
			return rBill;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
		}
		return null;
	}

	@Override
	public BillEntity toEntity(Bill rBill) throws AbstractException{
		try {
			if(rBill==null) throw new InputRequestException(AppConst.CODE_NULL_OBJECT,AppConst.MSG_NULL_OBJECT);			
			return new BillEntity(rBill.getIdBusiness(), rBill.getIdEmployee(), rBill.getIdClient(), rBill.getIdPayment(), 1, rBill.getObservations(), rBill.getRegister(), null, billingDetailsService.toEntityList(rBill.getBillingDetails()));
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<Bill> toDtoList(List<BillEntity> mList) {
		List<Bill> rList = new ArrayList<>();
		for (BillEntity billEntity : mList) {
			rList.add(toDto(billEntity));
		}
		return rList;
	}
}
