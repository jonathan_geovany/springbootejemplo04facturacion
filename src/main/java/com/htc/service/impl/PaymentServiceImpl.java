package com.htc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.dao.PaymentDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.exception.ServiceException;
import com.htc.model.PaymentEntity;
import com.htc.service.PaymentService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Component
public class PaymentServiceImpl implements PaymentService {
	
	private static final String DESCRIPTION = "description";
	private static final Logger log = LoggerFactory.getLogger(PaymentServiceImpl.class);

	@Autowired
	private MethodsSql methodsSql;
	@Autowired
	private PaymentDao paymentDao;
	
	@Override
	public void insert(PaymentEntity c) throws AbstractException{
		try {
			if(c==null || c.getDescription()==null || c.getDescription().isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+DESCRIPTION);
			Integer idPayment  = paymentDao.insertAndReturnKey(c);
			if(idPayment==null) throw new ServiceException(AppConst.CODE_CANT_INSERTED,AppConst.MSG_CANT_INSERTED+PaymentEntity.class.getSimpleName());
			c.setId(idPayment);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int[] insertList(List<PaymentEntity> list)throws AbstractException {
		try {
			for (PaymentEntity c : list) if(c==null || c.getDescription()==null || c.getDescription().isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+DESCRIPTION);
			return paymentDao.insertList(list);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean delete(PaymentEntity c) throws AbstractException{
		try {
			if(c==null||c.getId()==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");
			return paymentDao.delete(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<PaymentEntity> getAll()throws AbstractException {
		try {
			return paymentDao.getAll();
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public PaymentEntity getItemById(Integer id)throws AbstractException {
		try {
			if(id==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY + "id");	
			return paymentDao.getItemById(id);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int getCount()throws AbstractException {
		try {
			return paymentDao.getCount();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public boolean update(PaymentEntity c)throws AbstractException {
		try {
			if(c==null || c.getDescription()==null || c.getDescription().isEmpty()) throw new InputRequestException(AppConst.CODE_NULL_EMPTY, AppConst.MSG_NULL_EMPTY+DESCRIPTION);
			return paymentDao.update(c);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}
}
