package com.htc.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.billing.BillingDetail;
import com.htc.dao.BillingDetailsDao;
import com.htc.exception.AbstractException;
import com.htc.exception.InputRequestException;
import com.htc.model.BillingDetailEntity;
import com.htc.model.ProductEntity;
import com.htc.service.BillingDetailsService;
import com.htc.service.ProductService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Service
public class BillingDetailsServiceImpl implements BillingDetailsService {

	private static final Logger log = LoggerFactory.getLogger(BillingDetailsServiceImpl.class);
	
	@Autowired 
	private MethodsSql methodsSql;
	@Autowired 
	private BillingDetailsDao billingDetailsDao;
	@Autowired 
	private ProductService productService;
	
	
	@Override
	public List<BillingDetailEntity> getAll() throws AbstractException{
		try {
			return billingDetailsDao.getAll();
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}
	
	@Override
	public BillingDetailEntity getItemById(Integer id) throws AbstractException{
		try {
			if(id==null) throw  new InputRequestException( AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY+"id detalle" );
			return billingDetailsDao.getItemById(id);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public int getCount() throws AbstractException {
		try {
			return billingDetailsDao.getCount() ;
		} catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}


	@Override
	public BillingDetailEntity getBillingDetails(String productCode, Integer quantity) throws AbstractException{
		try {
			ProductEntity p = productService.getItemByCode(productCode);
			if(p==null) throw new InputRequestException( AppConst.CODE_NOT_FOUND_PRODUCT,AppConst.MSG_NOT_FOUND_PRODUCT+productCode );
			if(quantity > p.getStock()) throw new InputRequestException(AppConst.CODE_STOCK,AppConst.MSG_STOCK+p.getCode());
			if(quantity<=0) throw new InputRequestException(AppConst.CODE_NON_ZERO,AppConst.MSG_NON_ZERO+"cantidad de productos en detalle");
			BillingDetailEntity b = new BillingDetailEntity(p.getId(),p.getPrice(), quantity,p.getPrice().multiply(new BigDecimal(quantity)));
			b.setProduct(p);
			return b;
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public BillingDetailEntity getBillingDetails(Integer idProduct, Integer quantity) throws AbstractException {
		try {
			ProductEntity p = productService.getItemById(idProduct);
			if(p==null) throw new InputRequestException( AppConst.CODE_NOT_FOUND_PRODUCT,AppConst.MSG_NOT_FOUND_PRODUCT+idProduct );
			if(quantity > p.getStock()) throw new InputRequestException(AppConst.CODE_STOCK,AppConst.MSG_STOCK+p.getCode());
			if(quantity<=0) throw new InputRequestException(AppConst.CODE_NON_ZERO,AppConst.MSG_NON_ZERO+"cantidad de productos en detalle");
			BillingDetailEntity b = new BillingDetailEntity(p.getId(),p.getPrice(), quantity,p.getPrice().multiply(new BigDecimal(quantity)));
			b.setProduct(p);
			return b;
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<BillingDetailEntity> getAllInBill(Integer idBill) throws AbstractException {
		try {
			if(idBill==null) throw new InputRequestException(AppConst.CODE_NULL_EMPTY,AppConst.MSG_NULL_EMPTY+"id factura");
			return billingDetailsDao.getAllInBill(idBill);
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public BillingDetail toDto(BillingDetailEntity mDetail) throws AbstractException {
		try {
			if(mDetail==null) throw new InputRequestException(AppConst.CODE_NULL_OBJECT,AppConst.MSG_NULL_OBJECT);
			BillingDetail rDetail = new BillingDetail();
			rDetail.setId(mDetail.getId());
			rDetail.setIdBill(mDetail.getIdBill());
			rDetail.setIdProduct(mDetail.getIdProduct());
			rDetail.setPrice(mDetail.getPrice());
			rDetail.setQuantity(mDetail.getQuantity());
			rDetail.setTotal(mDetail.getTotal());
			return rDetail;
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public BillingDetailEntity toEntity(BillingDetail rDetail) throws AbstractException{
		try {
			if(rDetail==null) throw new InputRequestException(AppConst.CODE_NULL_OBJECT,AppConst.MSG_NULL_OBJECT);
			return getBillingDetails(rDetail.getIdProduct(), rDetail.getQuantity());
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<BillingDetail> toDtoList(List<BillingDetailEntity> mDetails)throws AbstractException {
		try {
			List<BillingDetail> rDetails = new ArrayList<>();
			for (BillingDetailEntity d : mDetails) rDetails.add(toDto(d));
			return rDetails;
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

	@Override
	public List<BillingDetailEntity> toEntityList(List<BillingDetail> rDetails) throws AbstractException{
		try {
			if(rDetails==null) throw new InputRequestException(AppConst.CODE_NULL_OBJECT,AppConst.MSG_NULL_OBJECT);
			List<BillingDetailEntity> mDetails = new ArrayList<>();
			for (BillingDetail d : rDetails) mDetails.add(toEntity(d));
			return mDetails;
		}catch (AbstractException e) {
			log.error(e.toString());
			throw e;
		}catch (Exception e) {
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
			throw e;
		}
	}

}
