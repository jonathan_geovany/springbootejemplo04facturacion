package com.htc.service;

import java.util.List;

import com.htc.exception.AbstractException;
import com.htc.model.ClientEntity;

public interface ClientService {
	public void insert(ClientEntity c) throws AbstractException;
	public boolean update(ClientEntity c) throws AbstractException;
	public int[] insertList(List<ClientEntity> list) throws AbstractException;
	public boolean delete(ClientEntity c) throws AbstractException;
	public List<ClientEntity> getAll() throws AbstractException;
	public ClientEntity getItemById(Integer id) throws AbstractException;
	public int getCount() throws AbstractException;
}
