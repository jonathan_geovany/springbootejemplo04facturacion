package com.htc.service;

import java.util.List;

import com.htc.billing.BillingDetail;
import com.htc.exception.AbstractException;
import com.htc.model.BillingDetailEntity;


public interface BillingDetailsService {
	
	public BillingDetailEntity getBillingDetails(String productCode,Integer quantity) throws AbstractException;
	public BillingDetailEntity getBillingDetails(Integer idProduct,Integer quantity) throws AbstractException;
	public List<BillingDetailEntity> getAll() throws AbstractException;
	public List<BillingDetailEntity> getAllInBill(Integer idBill) throws AbstractException;
	public BillingDetailEntity getItemById(Integer id) throws AbstractException;
	public int getCount() throws AbstractException;
	
	public BillingDetail toDto(BillingDetailEntity mDetail)throws AbstractException;
	public BillingDetailEntity toEntity(BillingDetail rDetail)throws AbstractException;
	public List<BillingDetail> toDtoList(List<BillingDetailEntity> mDetails)throws AbstractException;
	public List<BillingDetailEntity> toEntityList(List<BillingDetail> rDetails)throws AbstractException;
	
}
