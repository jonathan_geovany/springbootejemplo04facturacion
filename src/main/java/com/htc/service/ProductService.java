package com.htc.service;

import java.util.List;

import com.htc.billing.Product;
import com.htc.exception.AbstractException;
import com.htc.model.ProductEntity;

public interface ProductService {
	public void insert(ProductEntity p) throws AbstractException;
	public boolean update(ProductEntity p) throws AbstractException;
	public int[] insertList(List<ProductEntity> list) throws AbstractException;
	public boolean delete(ProductEntity p) throws AbstractException;
	public List<ProductEntity> getAll() throws AbstractException;
	public ProductEntity getItemById(Integer id) throws AbstractException;
	public ProductEntity getItemByCode(String code) throws AbstractException;
	public int getCount() throws AbstractException;
	
	public Product toDto(ProductEntity mProduct);
	public List<Product> toDtoList(List<ProductEntity> mProducts);
}
