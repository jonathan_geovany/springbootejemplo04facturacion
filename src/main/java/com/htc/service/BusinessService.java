package com.htc.service;

import java.util.List;

import com.htc.exception.AbstractException;
import com.htc.model.BusinessEntity;

public interface BusinessService {
	public void insert(BusinessEntity c) throws AbstractException;
	public boolean update(BusinessEntity c) throws AbstractException;
	public int[] insertList(List<BusinessEntity> list) throws AbstractException;
	public boolean delete(BusinessEntity c) throws AbstractException;
	public List<BusinessEntity> getAll() throws AbstractException;
	public BusinessEntity getItemById(Integer id) throws AbstractException;
	public int getCount() throws AbstractException;
}
