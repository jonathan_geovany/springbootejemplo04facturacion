//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.30 a las 11:49:22 AM CST 
//


package com.htc.billing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;



/**
 * <p>Clase Java para Bill complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Bill">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idBusiness" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idEmployee" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idClient" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idPayment" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="observations" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="register" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="emitted" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="tax" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="taxAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="total" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="BillingDetails" type="{http://billing.htc.com}BillingDetail" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bill", propOrder = {
    "id",
    "idBusiness",
    "idEmployee",
    "idClient",
    "idPayment",
    "idStatus",
    "observations",
    "register",
    "emitted",
    "tax",
    "taxAmount",
    "subtotal",
    "total",
    "billingDetails"
})
public class Bill {

    protected int id;
    protected int idBusiness;
    protected int idEmployee;
    protected int idClient;
    protected int idPayment;
    protected int idStatus;
    @XmlElement(required = true)
    protected String observations;
    @XmlElement(required = true)
    protected String register;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar emitted;
    @XmlElement(required = true)
    protected BigDecimal tax;
    @XmlElement(required = true)
    protected BigDecimal taxAmount;
    @XmlElement(required = true)
    protected BigDecimal subtotal;
    @XmlElement(required = true)
    protected BigDecimal total;
    @XmlElement(name = "BillingDetails", required = true)
    protected List<BillingDetail> billingDetails;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad idBusiness.
     * 
     */
    public int getIdBusiness() {
        return idBusiness;
    }

    /**
     * Define el valor de la propiedad idBusiness.
     * 
     */
    public void setIdBusiness(int value) {
        this.idBusiness = value;
    }

    /**
     * Obtiene el valor de la propiedad idEmployee.
     * 
     */
    public int getIdEmployee() {
        return idEmployee;
    }

    /**
     * Define el valor de la propiedad idEmployee.
     * 
     */
    public void setIdEmployee(int value) {
        this.idEmployee = value;
    }

    /**
     * Obtiene el valor de la propiedad idClient.
     * 
     */
    public int getIdClient() {
        return idClient;
    }

    /**
     * Define el valor de la propiedad idClient.
     * 
     */
    public void setIdClient(int value) {
        this.idClient = value;
    }

    /**
     * Obtiene el valor de la propiedad idPayment.
     * 
     */
    public int getIdPayment() {
        return idPayment;
    }

    /**
     * Define el valor de la propiedad idPayment.
     * 
     */
    public void setIdPayment(int value) {
        this.idPayment = value;
    }

    /**
     * Obtiene el valor de la propiedad idStatus.
     * 
     */
    public int getIdStatus() {
        return idStatus;
    }

    /**
     * Define el valor de la propiedad idStatus.
     * 
     */
    public void setIdStatus(int value) {
        this.idStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad observations.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservations() {
        return observations;
    }

    /**
     * Define el valor de la propiedad observations.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservations(String value) {
        this.observations = value;
    }

    /**
     * Obtiene el valor de la propiedad register.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegister() {
        return register;
    }

    /**
     * Define el valor de la propiedad register.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegister(String value) {
        this.register = value;
    }

    /**
     * Obtiene el valor de la propiedad emitted.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEmitted() {
        return emitted;
    }

    /**
     * Define el valor de la propiedad emitted.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEmitted(XMLGregorianCalendar value) {
        this.emitted = value;
    }

    /**
     * Obtiene el valor de la propiedad tax.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTax() {
        return tax;
    }

    /**
     * Define el valor de la propiedad tax.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTax(BigDecimal value) {
        this.tax = value;
    }

    /**
     * Obtiene el valor de la propiedad taxAmount.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    /**
     * Define el valor de la propiedad taxAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTaxAmount(BigDecimal value) {
        this.taxAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad subtotal.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSubtotal() {
        return subtotal;
    }

    /**
     * Define el valor de la propiedad subtotal.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSubtotal(BigDecimal value) {
        this.subtotal = value;
    }

    /**
     * Obtiene el valor de la propiedad total.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Define el valor de la propiedad total.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotal(BigDecimal value) {
        this.total = value;
    }

    /**
     * Gets the value of the billingDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the billingDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBillingDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BillingDetail }
     * 
     * 
     */
    public List<BillingDetail> getBillingDetails() {
        if (billingDetails == null) {
            billingDetails = new ArrayList<BillingDetail>();
        }
        return this.billingDetails;
    }

}
