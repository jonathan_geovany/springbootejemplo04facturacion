//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.30 a las 11:49:22 AM CST 
//


package com.htc.billing;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.htc.billing package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.htc.billing
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetBillByIdRequest }
     * 
     */
    public GetBillByIdRequest createGetBillByIdRequest() {
        return new GetBillByIdRequest();
    }

    /**
     * Create an instance of {@link InsertBillResponse }
     * 
     */
    public InsertBillResponse createInsertBillResponse() {
        return new InsertBillResponse();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link GetAllProductsResponse }
     * 
     */
    public GetAllProductsResponse createGetAllProductsResponse() {
        return new GetAllProductsResponse();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link SetNullBillResponse }
     * 
     */
    public SetNullBillResponse createSetNullBillResponse() {
        return new SetNullBillResponse();
    }

    /**
     * Create an instance of {@link SetNullBillRequest }
     * 
     */
    public SetNullBillRequest createSetNullBillRequest() {
        return new SetNullBillRequest();
    }

    /**
     * Create an instance of {@link GetAllBillsResponse }
     * 
     */
    public GetAllBillsResponse createGetAllBillsResponse() {
        return new GetAllBillsResponse();
    }

    /**
     * Create an instance of {@link Bill }
     * 
     */
    public Bill createBill() {
        return new Bill();
    }

    /**
     * Create an instance of {@link GetBillByIdResponse }
     * 
     */
    public GetBillByIdResponse createGetBillByIdResponse() {
        return new GetBillByIdResponse();
    }

    /**
     * Create an instance of {@link GetAllBillsRequest }
     * 
     */
    public GetAllBillsRequest createGetAllBillsRequest() {
        return new GetAllBillsRequest();
    }

    /**
     * Create an instance of {@link InsertBillRequest }
     * 
     */
    public InsertBillRequest createInsertBillRequest() {
        return new InsertBillRequest();
    }

    /**
     * Create an instance of {@link GetAllProductsRequest }
     * 
     */
    public GetAllProductsRequest createGetAllProductsRequest() {
        return new GetAllProductsRequest();
    }

    /**
     * Create an instance of {@link BillingDetail }
     * 
     */
    public BillingDetail createBillingDetail() {
        return new BillingDetail();
    }

}
