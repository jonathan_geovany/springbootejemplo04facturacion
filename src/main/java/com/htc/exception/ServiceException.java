package com.htc.exception;

public class ServiceException extends AbstractException {
	private static final long DEFAULT_CODE_EXCEPTION = 500;
	private static final long serialVersionUID = 1L;
	public ServiceException(Exception ex) {
		super(ex);
	}
	public ServiceException(long code, String description, Exception ex) {
		super(code, description, ex);
	}
	public ServiceException(long code, String description) {
		super(code, description);
	}
	public ServiceException(String description, Exception ex) {
		super(DEFAULT_CODE_EXCEPTION,description, ex);
	}
}
