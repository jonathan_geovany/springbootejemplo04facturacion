package com.htc.expose.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.htc.billing.Bill;
import com.htc.billing.GetAllBillsResponse;
import com.htc.billing.GetAllProductsResponse;
import com.htc.billing.GetBillByIdResponse;
import com.htc.billing.InsertBillResponse;
import com.htc.billing.Result;
import com.htc.billing.SetNullBillResponse;
import com.htc.exception.AbstractException;
import com.htc.expose.BillingExposing;
import com.htc.model.BillEntity;
import com.htc.service.BillService;
import com.htc.service.ProductService;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Component
public class BillingExposingImpl implements BillingExposing {
	
	private static final Logger log = LoggerFactory.getLogger(BillingExposingImpl.class);
	@Autowired
	private MethodsSql methodsSql;
	@Autowired
	private BillService billService;
	@Autowired
	private ProductService productService;
	
	@Override
	public InsertBillResponse insertBill(Bill rBill) {
		InsertBillResponse response = new InsertBillResponse();
		Result result = new Result();
		try {
			BillEntity mBill = billService.toEntity(rBill);
			billService.insert(mBill);			
			result.setCode(AppConst.DEFAULT_OK_CODE);
			result.setMsg(AppConst.OK_INSERT);
		}catch (AbstractException e) {
			result.setCode(e.getCode());
			if(e.getCode()>=AppConst.DEFAULT_FAIL_INPUT_CODE && e.getCode()<AppConst.DEFAULT_FAIL_SERVER_CODE ) {
				result.setMsg(e.getDescription());
			}else {
				result.setMsg(AppConst.FAIL_INSERT);
			}
			log.error(e.toString());
		}catch (Exception e) {
			result.setCode(AppConst.DEFAULT_FAIL_SERVER_CODE);
			result.setMsg(AppConst.FAIL_INSERT);
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
		}
		response.setResult(result);
		return response;
	}

	@Override
	public GetAllBillsResponse getAllBills() {
		GetAllBillsResponse response = new GetAllBillsResponse();
		Result result = new Result();
		try {
			List<Bill> rBills = billService.toDtoList( billService.getAllBills() );
			response.getBills().addAll(rBills);
			result.setCode(AppConst.DEFAULT_OK_CODE);
			result.setMsg(AppConst.OK_QUERY);
		}catch (AbstractException e) {
			result.setCode(e.getCode());
			if(e.getCode()>=AppConst.DEFAULT_FAIL_INPUT_CODE && e.getCode()<AppConst.DEFAULT_FAIL_SERVER_CODE ) {
				result.setMsg(e.getDescription());
			}else {
				result.setMsg(AppConst.FAIL_QUERY);
			}
			log.error(e.toString());
		}catch (Exception e) {
			result.setCode(AppConst.DEFAULT_FAIL_SERVER_CODE);
			result.setMsg(AppConst.FAIL_QUERY);
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
		}
		response.setResult(result);
		return response;
	}

	@Override
	public GetBillByIdResponse getBillById(Integer id) {
		GetBillByIdResponse response = new GetBillByIdResponse();
		Result result = new Result();
		try {
			response.setBill( billService.toDto( billService.getBillById(id)));
			result.setCode(AppConst.DEFAULT_OK_CODE);
			result.setMsg(AppConst.OK_QUERY);
		}catch (AbstractException e) {
			result.setCode(e.getCode());
			if(e.getCode()>=AppConst.DEFAULT_FAIL_INPUT_CODE && e.getCode()<AppConst.DEFAULT_FAIL_SERVER_CODE ) {
				result.setMsg(e.getDescription());
			}else {
				result.setMsg(AppConst.FAIL_QUERY);
			}
			log.error(e.toString());
		}catch (Exception e) {
			result.setCode(AppConst.DEFAULT_FAIL_SERVER_CODE);
			result.setMsg(AppConst.FAIL_QUERY);
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
		}
		response.setResult(result);
		return response;
	}

	@Override
	public SetNullBillResponse setNullBill(Integer id, String observation) {
		SetNullBillResponse response = new SetNullBillResponse();
		Result result = new Result();
		try {
			BillEntity billEntity = new BillEntity();
			billEntity.setId(id);
			billEntity.setObservations(observation);
			billService.setNullBill(billEntity);
			result.setCode(AppConst.DEFAULT_OK_CODE);
			result.setMsg(AppConst.OK_NULL);
		} 
		catch (AbstractException e) {
			result.setCode(e.getCode());
			if(e.getCode()>=AppConst.DEFAULT_FAIL_INPUT_CODE && e.getCode()<AppConst.DEFAULT_FAIL_SERVER_CODE ) {
				result.setMsg(e.getDescription());
			}else {
				result.setMsg(AppConst.FAIL_NULL);
			}
			log.error(e.toString());
		}
		catch (Exception e) {
			result.setCode(AppConst.DEFAULT_FAIL_SERVER_CODE);
			result.setMsg(AppConst.FAIL_NULL);
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
		}
		response.setResult(result);
		return response;
	}

	@Override
	public GetAllProductsResponse getAllProducts() {
		GetAllProductsResponse response = new GetAllProductsResponse();
		Result result = new Result();
		try {
			response.getProducts().addAll(  productService.toDtoList(productService.getAll()));
			result.setCode(AppConst.DEFAULT_OK_CODE);
			result.setMsg(AppConst.OK_QUERY);
		} 
		catch (AbstractException e) {
			result.setCode(e.getCode());
			if(e.getCode()>=AppConst.DEFAULT_FAIL_INPUT_CODE && e.getCode()<AppConst.DEFAULT_FAIL_SERVER_CODE ) {
				result.setMsg(e.getDescription());
			}else {
				result.setMsg(AppConst.FAIL_QUERY);
			}
			log.error(e.toString());
		}
		catch (Exception e) {
			result.setCode(AppConst.DEFAULT_FAIL_SERVER_CODE);
			result.setMsg(AppConst.FAIL_QUERY);
			log.error(methodsSql.msg(e,new Exception().getStackTrace()[0]));
		}
		response.setResult(result);
		return response;
	}

}
