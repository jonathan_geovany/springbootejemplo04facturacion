package com.htc.expose;

import com.htc.billing.Bill;
import com.htc.billing.GetAllBillsResponse;
import com.htc.billing.GetAllProductsResponse;
import com.htc.billing.GetBillByIdResponse;
import com.htc.billing.InsertBillResponse;
import com.htc.billing.SetNullBillResponse;

public interface BillingExposing {
	public InsertBillResponse insertBill(Bill rBill);
	public GetAllBillsResponse getAllBills();
	public GetBillByIdResponse getBillById(Integer id);
	public SetNullBillResponse setNullBill(Integer id,String observation);
	public GetAllProductsResponse getAllProducts();
}
