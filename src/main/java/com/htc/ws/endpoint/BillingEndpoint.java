package com.htc.ws.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.billing.GetAllBillsRequest;
import com.htc.billing.GetAllBillsResponse;
import com.htc.billing.GetAllProductsRequest;
import com.htc.billing.GetAllProductsResponse;
import com.htc.billing.GetBillByIdRequest;
import com.htc.billing.GetBillByIdResponse;
import com.htc.billing.InsertBillRequest;
import com.htc.billing.InsertBillResponse;
import com.htc.billing.SetNullBillRequest;
import com.htc.billing.SetNullBillResponse;
import com.htc.expose.BillingExposing;

@Endpoint
public class BillingEndpoint {
	
	private static final String NAMESPACE_URI = "http://billing.htc.com";

	@Autowired
	private BillingExposing billingExposing;
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "InsertBillRequest")
	@ResponsePayload
	public InsertBillResponse insertBill(@RequestPayload InsertBillRequest request) {
		return billingExposing.insertBill(request.getBill());
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllBillsRequest")
	@ResponsePayload
	public GetAllBillsResponse getAllBills(@RequestPayload GetAllBillsRequest request) {
		return billingExposing.getAllBills();
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetBillByIdRequest")
	@ResponsePayload
	public GetBillByIdResponse getBillById(@RequestPayload GetBillByIdRequest request) {
		return billingExposing.getBillById(request.getId());
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "SetNullBillRequest")
	@ResponsePayload
	public SetNullBillResponse setNullBill(@RequestPayload SetNullBillRequest request) {
		return billingExposing.setNullBill(request.getId(), request.getObservation());
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetAllProductsRequest")
	@ResponsePayload
	public GetAllProductsResponse getAllProducts(@RequestPayload GetAllProductsRequest request) {
		return billingExposing.getAllProducts();
	}
	
}
