package com.htc.dao;

import java.util.List;

import com.htc.model.BusinessEntity;

public interface BusinessDao {
	public boolean insert(BusinessEntity e);
	public Integer insertAndReturnKey(BusinessEntity e);
	public boolean update(BusinessEntity e);
	public int[] insertList(List<BusinessEntity> list);
	public boolean delete(BusinessEntity e);
	public List<BusinessEntity> getAll();
	public BusinessEntity getItemById(Integer id);
	public int getCount();
}
