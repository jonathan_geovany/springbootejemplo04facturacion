package com.htc.dao;

import java.util.List;
import java.util.Map;

import com.htc.model.BillEntity;

public interface BillDao {
	/**
	 * Inserta una factura valida
	 * @param f factura que contiene id de FK necesarias para relacionar la informacion
	 * @return true si se inserto correctamente, false si fallo
	 */
	public boolean insert(BillEntity f);
	
	/**
	 * inserta una factura valida y retorna la llave de esa factura
	 * @param c factura que contiene id de FK necesarias para relacionar la informacion
	 * @return llave de la factura insertada o null si no se pudo insertar
	 */
	public Integer insertAndReturnKey(BillEntity c);
	/**
	 * actualiza unicamente informacion no sensible en la factura
	 * @param f solamente se actualiza las observaciones de la factura
	 * @return true si se actualizo correctamente, false si fallo
	 */
	public boolean update(BillEntity f);
	/**
	 * inserta una lista de facturas de manera que sea una operacion transaccional
	 * @param list lista de facturas
	 * @return elementos afectados
	 */
	public int[] insertList(List<BillEntity> list);
	public boolean setNullBill(BillEntity f);
	public List<BillEntity> getAllBills();
	public List<BillEntity> getNonNullBills();
	
	public List<Map<String, Object>> getQuery(String query);
	public BillEntity getBillById(Integer id);
	public int getCount();
}
