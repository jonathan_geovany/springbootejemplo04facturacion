package com.htc.dao;

import java.util.List;

import com.htc.model.EmployeeEntity;

public interface EmployeeDao {
	public boolean insert(EmployeeEntity e);
	public Integer insertAndReturnKey(EmployeeEntity e);
	public boolean update(EmployeeEntity e);
	public int[] insertList(List<EmployeeEntity> list);
	public boolean delete(EmployeeEntity e);
	public List<EmployeeEntity> getAll();
	public EmployeeEntity getItemById(Integer id);
	public int getCount();
}
