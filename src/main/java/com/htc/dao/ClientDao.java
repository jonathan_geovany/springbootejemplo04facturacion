package com.htc.dao;

import java.util.List;

import com.htc.model.ClientEntity;

public interface ClientDao {
	public boolean insert(ClientEntity c);
	public Integer insertAndReturnKey(ClientEntity c);
	public boolean update(ClientEntity c);
	public int[] insertList(List<ClientEntity> list);
	public boolean delete(ClientEntity c);
	public List<ClientEntity> getAll();
	public ClientEntity getItemById(Integer id);
	public int getCount();
}
