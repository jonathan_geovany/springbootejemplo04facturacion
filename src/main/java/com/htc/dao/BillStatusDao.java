package com.htc.dao;

import java.util.List;

import com.htc.model.BillStatusEntity;

public interface BillStatusDao {
	public boolean insert(BillStatusEntity c);
	public Integer insertAndReturnKey(BillStatusEntity c);
	public boolean update(BillStatusEntity c);
	public int[] insertList(List<BillStatusEntity> list);
	public boolean delete(BillStatusEntity c);
	public List<BillStatusEntity> getAll();
	public BillStatusEntity getItemById(Integer id);
	public int getCount();
}
