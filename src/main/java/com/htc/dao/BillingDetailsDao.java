package com.htc.dao;

import java.util.List;

import com.htc.model.BillingDetailEntity;

public interface BillingDetailsDao {
	public boolean insert(BillingDetailEntity d);
	public Integer insertAndReturnKey(BillingDetailEntity c);
	public boolean update(BillingDetailEntity d);
	public int[] insertList(List<BillingDetailEntity> list);
	public int[] insertList(List<BillingDetailEntity> list,Integer idBill);
	
	public boolean delete(BillingDetailEntity c);
	public List<BillingDetailEntity> getAll();
	public List<BillingDetailEntity> getAllInBill(Integer idBill);
	
	public BillingDetailEntity getItemById(Integer id);
	public int getCount();
}
