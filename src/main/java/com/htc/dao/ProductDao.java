package com.htc.dao;

import java.util.List;

import com.htc.model.ProductEntity;

public interface ProductDao {
	public boolean insert(ProductEntity p);
	public Integer insertAndReturnKey(ProductEntity p);
	public boolean update(ProductEntity p);
	public int[]   insertList(List<ProductEntity> list);
	public boolean updateList(List<ProductEntity> list);
	public boolean delete(ProductEntity p);
	public List<ProductEntity> getAll();
	public ProductEntity getItemById(Integer id);
	public ProductEntity getItemByCode(String code);
	public int getCount();
}
