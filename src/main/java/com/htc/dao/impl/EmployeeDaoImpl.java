package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.htc.dao.EmployeeDao;
import com.htc.mapper.EmployeeRowMapper;
import com.htc.model.EmployeeEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Repository
public class EmployeeDaoImpl extends JdbcDaoSupport implements EmployeeDao {


	@Autowired
	private DataSource dataSource;
	@Autowired
	private MethodsSql methodsSql;

	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(EmployeeEntity e) {
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_EMPLOYEE,
					AppConst.VALUES_EMPLOYEE,
					true
				),
				new Object[] 
				{
					e.getIdBusiness(),
					e.getNames(),
					e.getDui(),
					e.getPosition(),
					e.getSalary(),
					e.getPhone(),
					e.getSalary(),
					e.getEmail()
				})>0;
	}

	@Override
	public int[] insertList(List<EmployeeEntity> list) {
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_EMPLOYEE,
						AppConst.VALUES_EMPLOYEE,
						false
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						EmployeeEntity c = list.get(arg1);
			
						
						arg0.setInt		(1, c.getIdBusiness());
						arg0.setString	(2, c.getNames());
						arg0.setString	(3, c.getDui());
						arg0.setString	(4, c.getPosition());
						arg0.setBigDecimal	(5, c.getSalary());
						arg0.setString	(6, c.getPhone());
						arg0.setString	(7, c.getAddress());
						arg0.setString	(8, c.getEmail());
					}
				}
			   );
	}

	@Override
	public boolean delete(EmployeeEntity e) {
		return getJdbcTemplate().update(
				methodsSql.delete(
						AppConst.TABLE_EMPLOYEE,
						AppConst.ID_EMPLOYEE
				),
				e.getId()
				)>0;
	}

	@Override
	public List<EmployeeEntity> getAll() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_EMPLOYEE), 
				new EmployeeRowMapper()
				);
	}

	@Override
	public EmployeeEntity getItemById(Integer id) {
		List<EmployeeEntity> list = getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_EMPLOYEE,
						AppConst.ID_EMPLOYEE
						),
				new Object[] {id},
				new EmployeeRowMapper()
				);
		if(list.isEmpty()) return null;
		return list.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_EMPLOYEE), 
				Integer.class);
	}

	@Override
	public boolean update(EmployeeEntity e) {
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_EMPLOYEE,
						AppConst.VALUES_EMPLOYEE,
						AppConst.ID_EMPLOYEE
					),
					new Object[] 
					{
						e.getIdBusiness(),
						e.getNames(),
						e.getDui(),
						e.getPosition(),
						e.getSalary(),
						e.getPhone(),
						e.getAddress(),
						e.getEmail(),
						e.getId(),//al final el id de la tabla
					})>0;
	}

	@Override
	public Integer insertAndReturnKey(EmployeeEntity c) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement arg0 = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_EMPLOYEE,
									AppConst.VALUES_EMPLOYEE,
									true)
							, 
							new String[] {"id"}
							);

					arg0.setInt		(1, c.getIdBusiness());
					arg0.setString	(2, c.getNames());
					arg0.setString	(3, c.getDui());
					arg0.setString	(4, c.getPosition());
					arg0.setBigDecimal	(5, c.getSalary());
					arg0.setString	(6, c.getPhone());
					arg0.setString	(7, c.getAddress());
					arg0.setString	(8, c.getEmail());
			        return arg0;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
	}

}
