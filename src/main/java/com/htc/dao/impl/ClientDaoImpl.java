package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.dao.ClientDao;
import com.htc.mapper.ClientRowMapper;
import com.htc.model.ClientEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Repository
public class ClientDaoImpl extends JdbcDaoSupport implements ClientDao {

	@Autowired
	private DataSource dataSource;
	@Autowired
	private MethodsSql methodsSql;

	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(ClientEntity c) {
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_CLIENT,
					AppConst.VALUES_CLIENT,
					true
				),
				new Object[] 
				{
					c.getNames(),
					c.getNit(),
					c.getDui(),
					c.getPhone(),
					c.getAddress()
				})>0;
	}

	@Transactional //o se hacen todos o no se hace ninguno
	@Override
	public int[] insertList(List<ClientEntity> list) {
		
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_CLIENT,
						AppConst.VALUES_CLIENT,
						false
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						ClientEntity c = list.get(arg1);
			
						arg0.setString(1, c.getNames());
						arg0.setString(2, c.getNit());
						arg0.setString(3, c.getDui());
						arg0.setString(4, c.getPhone());
						arg0.setString(5, c.getAddress());
					}
				}
			   );
		 
	}

	@Override
	public boolean delete(ClientEntity c) {
		
		return getJdbcTemplate().update(
				methodsSql.delete(
						AppConst.TABLE_CLIENT,
						AppConst.ID_CLIENT
				),
				c.getId()
				)>0;
	}

	@Override
	public List<ClientEntity> getAll() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_CLIENT), 
				new ClientRowMapper()
				);
	}

	@Override
	public ClientEntity getItemById(Integer id) {
		List<ClientEntity> list = getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_CLIENT,
						AppConst.ID_CLIENT
						),
				new Object[] {id},
				new ClientRowMapper()
				);
		if(list.isEmpty()) return null;
		return list.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_CLIENT), 
				Integer.class);
	}

	@Override
	public boolean update(ClientEntity c) {
		
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_CLIENT,
						AppConst.VALUES_CLIENT,
						AppConst.ID_CLIENT
					),
					new Object[] 
					{
						c.getNames(),
						c.getNit(),
						c.getDui(),
						c.getPhone(),
						c.getAddress(),
						c.getId(),//al final el id de la tabla
					})>0;
	}

	@Override
	public Integer insertAndReturnKey(ClientEntity c) {
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement ps = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_CLIENT,
									AppConst.VALUES_CLIENT,
									true)
							, 
							new String[] {"id"}
							);
					
					ps.setString(1, c.getNames());
					ps.setString(2, c.getNit());
					ps.setString(3, c.getDui());
					ps.setString(4, c.getPhone());
					ps.setString(5, c.getAddress());
			        return ps;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
		
	}

}
