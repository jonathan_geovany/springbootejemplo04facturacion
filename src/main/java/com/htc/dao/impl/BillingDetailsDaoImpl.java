package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.dao.BillingDetailsDao;
import com.htc.mapper.BillingDetailsRowMapper;
import com.htc.model.BillingDetailEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Repository
public class BillingDetailsDaoImpl extends JdbcDaoSupport implements BillingDetailsDao {

	@Autowired
	private DataSource dataSource;
	@Autowired
	private MethodsSql methodsSql;
	
	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(BillingDetailEntity d) {
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_BILLING_DETAILS,
					AppConst.VALUES_BILLING_DETAILS,
					true
				),
				new Object[] 
				{
					d.getIdBill(),
					d.getIdProduct(),
					d.getPrice(),
					d.getQuantity(),
					d.getTotal()
				})>0;
	}

	@Transactional
	@Override
	public int[] insertList(List<BillingDetailEntity> list) {
		
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_BILLING_DETAILS,
						AppConst.VALUES_BILLING_DETAILS,
						true
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						BillingDetailEntity c = list.get(arg1);
			
						arg0.setInt			(1, c.getIdBill());
						arg0.setInt			(2, c.getIdProduct());
						arg0.setBigDecimal	(3, c.getPrice());
						arg0.setInt			(4, c.getQuantity());
						arg0.setBigDecimal	(5, c.getTotal());
					}
				}
			   );
	}

	@Override
	public boolean delete(BillingDetailEntity c) {
		return getJdbcTemplate().update(
				methodsSql.delete(
						AppConst.TABLE_BILLING_DETAILS,
						AppConst.ID_BILLING_DETAILS
				),
				c.getId()
				)>0;
	}

	@Override
	public List<BillingDetailEntity> getAll() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_BILLING_DETAILS), 
				new BillingDetailsRowMapper()
				);
	}

	@Override
	public BillingDetailEntity getItemById(Integer id) {
		List<BillingDetailEntity> list = getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_BILLING_DETAILS,
						AppConst.ID_BILLING_DETAILS
						),
				new Object[] {id},
				new BillingDetailsRowMapper()
				);
		if(list.isEmpty()) return null;
		return list.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_BILLING_DETAILS), 
				Integer.class);
	}

	@Override
	public boolean update(BillingDetailEntity c) {
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_CLIENT,
						AppConst.VALUES_CLIENT,
						AppConst.ID_CLIENT
					),
					new Object[] 
					{
						c.getIdBill(),
						c.getIdProduct(),
						c.getPrice(),
						c.getQuantity(),
						c.getId(),//al final el id de la tabla
					})>0;
	}

	@Transactional
	@Override
	public int[] insertList(List<BillingDetailEntity> list, Integer idBill) {
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_BILLING_DETAILS,
						AppConst.VALUES_BILLING_DETAILS,
						true
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						BillingDetailEntity c = list.get(arg1);
			
						arg0.setInt			(1, idBill);
						arg0.setInt			(2, c.getIdProduct());
						arg0.setBigDecimal	(3, c.getPrice());
						arg0.setInt			(4, c.getQuantity());
						arg0.setBigDecimal	(5, c.getTotal());
					}
				}
			   );
	}

	@Override
	public List<BillingDetailEntity> getAllInBill(Integer idBill) {
		return getJdbcTemplate().query(
				AppConst.FILTER_BILLING_DETAILS,
				new Object[] {idBill},
				new BillingDetailsRowMapper()
				);
	}

	@Override
	public Integer insertAndReturnKey(BillingDetailEntity c) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement arg0 = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_BILLING_DETAILS,
									AppConst.VALUES_BILLING_DETAILS,
									true)
							, 
							new String[] {"id"}
							);

					arg0.setInt			(1, c.getIdBill());
					arg0.setInt			(2, c.getIdProduct());
					arg0.setBigDecimal	(3, c.getPrice());
					arg0.setInt			(4, c.getQuantity());
					arg0.setBigDecimal	(5, c.getTotal());
			        return arg0;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
	}

}
