package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.dao.BillDao;
import com.htc.mapper.BillRowMapper;
import com.htc.model.BillEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;


@Repository
public class BillDaoImpl extends JdbcDaoSupport implements BillDao {


	@Autowired
	private DataSource dataSource;
	@Autowired
	private MethodsSql methodsSql;

	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(BillEntity f) {
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_BILL,
					AppConst.VALUES_BILL,
					true
				),
				new Object[] 
				{
					f.getIdBusiness(),
					f.getIdEmployee(),
					f.getIdClient(),
					f.getIdPayment(),
					f.getIdStatus(),
					f.getObservations(),
					f.getRegister(),
					f.getEmitted(),
					f.getTax(),
					f.getTaxAmount(),
					f.getSubtotal(),
					f.getTotal()
				})>0;
	}

	
	
	@Transactional
	@Override
	public int[] insertList(List<BillEntity> list) {
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_BILL,
						AppConst.VALUES_BILL,
						false
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						BillEntity c = list.get(arg1);
				
						arg0.setInt		 (1,  c.getIdBusiness());
						arg0.setInt		 (2,  c.getIdEmployee());
						arg0.setInt		 (3,  c.getIdClient());
						arg0.setInt		 (4,  c.getIdPayment());
						arg0.setInt		 (5,  c.getIdStatus());
						arg0.setString	 (6,  c.getObservations());
						arg0.setString	 (7,  c.getRegister());
						arg0.setTimestamp(8,  c.getEmitted());
						arg0.setBigDecimal	 (9,  c.getTax());
						arg0.setBigDecimal	 (10,  c.getTaxAmount());
						arg0.setBigDecimal	 (11,  c.getSubtotal());
						arg0.setBigDecimal	 (12, c.getTotal());
					}
				}
			   );
	}
	

	
	@Override
	public boolean setNullBill(BillEntity f) {
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_BILL,
						AppConst.VALUES_BILL_NULL,
						AppConst.ID_BILL
					),
					new Object[] 
					{
							
						f.getIdStatus(),
						f.getObservations(),
						f.getId()//id al final
						
					})>0;
	}

	@Override
	public List<BillEntity> getAllBills() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_BILL), 
				new BillRowMapper()
				);
	}
	
	@Override
	public List<Map<String, Object>> getQuery(String query) {
		return getJdbcTemplate().queryForList(query);
	}

	@Override
	public BillEntity getBillById(Integer id) {
		List<BillEntity> list = getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_BILL,
						AppConst.ID_BILL
						),
				new Object[] {id},
				new BillRowMapper()
				);
		if(list.isEmpty()) return null;
		return list.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_BILL), 
				Integer.class);
	}

	
	@Override
	public boolean update(BillEntity f) {
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_BILL,
						AppConst.VALUES_BILL_UPDATE,
						AppConst.ID_BILL
					),
					new Object[] 
					{
							f.getObservations(),
							f.getId()//id al final
					})>0;
	}

	
	@Override
	public Integer insertAndReturnKey(BillEntity c) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement arg0 = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_BILL,
									AppConst.VALUES_BILL,
									true)
							, 
							new String[] {"id"}
							);

					arg0.setInt		 (1,  c.getIdBusiness());
					arg0.setInt		 (2,  c.getIdEmployee());
					arg0.setInt		 (3,  c.getIdClient());
					arg0.setInt		 (4,  c.getIdPayment());
					arg0.setInt		 (5,  c.getIdStatus());
					arg0.setString	 (6,  c.getObservations());
					arg0.setString	 (7,  c.getRegister());
					arg0.setTimestamp(8,  c.getEmitted());
					arg0.setBigDecimal	 (9,  c.getTax());
					arg0.setBigDecimal	 (10,  c.getTaxAmount());
					arg0.setBigDecimal	 (11,  c.getSubtotal());
					arg0.setBigDecimal	 (12, c.getTotal());
			        return arg0;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
	}

	
	@Override
	public List<BillEntity> getNonNullBills() {
		return getJdbcTemplate().query(
				AppConst.NON_NULL_BILLS,
				new BillRowMapper()
				);
	}

}
