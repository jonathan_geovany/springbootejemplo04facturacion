package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.htc.dao.BusinessDao;
import com.htc.mapper.BusinessRowMapper;
import com.htc.model.BusinessEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Repository
public class BusinessDaoImpl extends JdbcDaoSupport implements BusinessDao {

	@Autowired
	private DataSource dataSource;
	@Autowired
	private MethodsSql methodsSql;

	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(BusinessEntity e) {
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_BUSINESS,
					AppConst.VALUES_BUSINESS,
					true
				),
				new Object[] 
				{
					e.getTradename(),
					e.getBusinessName(),
					e.getSector(),
					e.getNit(),
					e.getNrc(),
					e.getCountry(),
					e.getCity(),
					e.getAddress(),
					e.getPhone1(),
					e.getPhone2(),
					e.getEmail(),
					e.getTax()
				})>0;
	}

	@Override
	public int[] insertList(List<BusinessEntity> list) {
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_BUSINESS,
						AppConst.VALUES_BUSINESS,
						false
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						BusinessEntity c = list.get(arg1);
				
						arg0.setString		(1,  c.getTradename());
						arg0.setString		(2,  c.getBusinessName());
						arg0.setString		(3,  c.getSector());
						arg0.setString		(4,  c.getNit());
						arg0.setString		(5,  c.getNrc());
						arg0.setString		(6,  c.getCountry());
						arg0.setString		(7,  c.getCity());
						arg0.setString		(8,  c.getAddress());
						arg0.setString		(9,  c.getPhone1());
						arg0.setString		(10, c.getPhone2());
						arg0.setString		(11, c.getEmail());
						arg0.setBigDecimal	(12, c.getTax());
					}
				}
			   );
	}

	@Override
	public boolean delete(BusinessEntity e) {
		return getJdbcTemplate().update(
				methodsSql.delete(
						AppConst.TABLE_BUSINESS,
						AppConst.ID_BUSINESS
				),
				e.getId()
				)>0;
	}

	@Override
	public List<BusinessEntity> getAll() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_BUSINESS), 
				new BusinessRowMapper()
				);
	}

	@Override
	public BusinessEntity getItemById(Integer id) {
		List<BusinessEntity> list = getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_BUSINESS,
						AppConst.ID_BUSINESS
						),
				new Object[] {id},
				new BusinessRowMapper()
				);
		if(list.isEmpty()) return null;
		return list.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_BUSINESS), 
				Integer.class);
	}

	@Override
	public boolean update(BusinessEntity e) {
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_BUSINESS,
						AppConst.VALUES_BUSINESS,
						AppConst.ID_BUSINESS
					),
					new Object[] 
					{
							
						e.getTradename(),
						e.getBusinessName(),
						e.getSector(),
						e.getNit(),
						e.getNrc(),
						e.getCountry(),
						e.getCity(),
						e.getAddress(),
						e.getPhone1(),
						e.getPhone2(),
						e.getEmail(),
						e.getTax(),
						e.getId(),//al final el id de la tabla
					})>0;
	}

	@Override
	public Integer insertAndReturnKey(BusinessEntity c) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement arg0 = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_BUSINESS,
									AppConst.VALUES_BUSINESS,
									true)
							, 
							new String[] {"id"}
							);

					arg0.setString		(1,  c.getTradename());
					arg0.setString		(2,  c.getBusinessName());
					arg0.setString		(3,  c.getSector());
					arg0.setString		(4,  c.getNit());
					arg0.setString		(5,  c.getNrc());
					arg0.setString		(6,  c.getCountry());
					arg0.setString		(7,  c.getCity());
					arg0.setString		(8,  c.getAddress());
					arg0.setString		(9,  c.getPhone1());
					arg0.setString		(10, c.getPhone2());
					arg0.setString		(11, c.getEmail());
					arg0.setBigDecimal	(12, c.getTax());
			        return arg0;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
	}

}
