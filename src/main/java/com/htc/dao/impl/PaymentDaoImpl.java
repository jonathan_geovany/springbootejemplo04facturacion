package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.dao.PaymentDao;
import com.htc.mapper.PaymentRowMapper;
import com.htc.model.PaymentEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Repository
public class PaymentDaoImpl extends JdbcDaoSupport implements PaymentDao {

	@Autowired
	private DataSource dataSource;
	@Autowired
	private MethodsSql methodsSql;

	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(PaymentEntity c) {
		
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_PAYMENT,
					AppConst.VALUES_PAYMENT,
					true
				),
				new Object[] 
				{
					c.getDescription()
				})>0;
	}

	@Transactional
	@Override
	public int[] insertList(List<PaymentEntity> list) {

		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_PAYMENT,
						AppConst.VALUES_PAYMENT,
						true
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						PaymentEntity c = list.get(arg1);
						arg0.setString(1, c.getDescription());
					}
				}
			   );
	}

	@Override
	public boolean delete(PaymentEntity c) {
		return getJdbcTemplate().update(
				methodsSql.delete(
						AppConst.TABLE_PAYMENT,
						AppConst.ID_PAYMENT
				),
				c.getId()
				)>0;
	}

	@Override
	public List<PaymentEntity> getAll() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_PAYMENT), 
				new PaymentRowMapper()
				);
	}

	@Override
	public PaymentEntity getItemById(Integer id) {
		List<PaymentEntity> list =  getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_PAYMENT,
						AppConst.ID_PAYMENT
						),
				new Object[] {id},
				new PaymentRowMapper()
				);
		if(list.isEmpty()) return null;
		return list.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_PAYMENT), 
				Integer.class);
	}

	@Override
	public boolean update(PaymentEntity p) {

		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_PAYMENT,
						AppConst.VALUES_PAYMENT,
						AppConst.ID_PAYMENT
					),
					new Object[] 
					{
						p.getDescription(),
						p.getId(),//al final el id de la tabla
					})>0;
	}

	@Override
	public Integer insertAndReturnKey(PaymentEntity c) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement arg0 = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_PAYMENT,
									AppConst.VALUES_PAYMENT,
									true)
							, 
							new String[] {"id"}
							);

					arg0.setString(1, c.getDescription());
			        return arg0;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
	}

}
