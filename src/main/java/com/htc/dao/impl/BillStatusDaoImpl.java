package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.htc.dao.BillStatusDao;
import com.htc.mapper.BillStatusRowMapper;
import com.htc.model.BillStatusEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;
@Repository
public class BillStatusDaoImpl extends JdbcDaoSupport implements BillStatusDao {
	
	@Autowired
	private DataSource dataSource;
	@Autowired 
	private MethodsSql methodsSql;
	
	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(BillStatusEntity c) {
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_BILL_STATUS,
					AppConst.VALUES_BILL_STATUS,
					true
				),
				new Object[] 
				{
					c.getDescription()
				})>0;
	}

	@Override
	public boolean update(BillStatusEntity p) {
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_BILL_STATUS,
						AppConst.VALUES_BILL_STATUS,
						AppConst.ID_BILL_STATUS
					),
					new Object[] 
					{
						p.getDescription(),
						p.getId(),//al final el id de la tabla
					})>0;
	}

	@Override
	public int[] insertList(List<BillStatusEntity> list) {
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_BILL_STATUS,
						AppConst.VALUES_BILL_STATUS,
						true
				),
				new BatchPreparedStatementSetter() {

					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						BillStatusEntity c = list.get(arg1);
						arg0.setString(1, c.getDescription());
					}
				}
			   );
	}

	@Override
	public boolean delete(BillStatusEntity c) {
		return getJdbcTemplate().update(
				methodsSql.delete(
						AppConst.TABLE_BILL_STATUS,
						AppConst.ID_BILL_STATUS
				),
				c.getId()
				)>0;
	}

	@Override
	public List<BillStatusEntity> getAll() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_BILL_STATUS), 
				new BillStatusRowMapper()
				);
	}

	@Override
	public BillStatusEntity getItemById(Integer id) {
		List<BillStatusEntity> list =  getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_BILL_STATUS,
						AppConst.ID_BILL_STATUS
						),
				new Object[] {id},
				new BillStatusRowMapper()
				);
		if(list.isEmpty()) return null;
		return list.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_BILL_STATUS), 
				Integer.class);
	}

	@Override
	public Integer insertAndReturnKey(BillStatusEntity c) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement arg0 = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_BILL_STATUS,
									AppConst.VALUES_BILL_STATUS,
									true)
							, 
							new String[] {"id"}
							);

					arg0.setString(1, c.getDescription());
			        return arg0;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
	}

}
