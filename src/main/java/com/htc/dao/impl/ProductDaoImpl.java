package com.htc.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.dao.ProductDao;
import com.htc.mapper.ProductRowMapper;
import com.htc.model.ProductEntity;
import com.htc.utils.AppConst;
import com.htc.utils.interfaces.MethodsSql;

@Repository
public class ProductDaoImpl extends JdbcDaoSupport implements ProductDao {

	@Autowired DataSource dataSource;
	
	@Autowired MethodsSql methodsSql;

	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(ProductEntity p) {
		return getJdbcTemplate().update(
				methodsSql.insert(
					AppConst.TABLE_PRODUCT,
					AppConst.VALUES_PRODUCT,
					true
				),
				new Object[] 
				{
					p.getCode(),
					p.getDescription(),
					p.getStock(),
					p.getPrice()
				})>0;
	}

	
	@Transactional
	@Override
	public int[] insertList(List<ProductEntity> list) {
		return getJdbcTemplate().batchUpdate(
				methodsSql.insert(
						AppConst.TABLE_PRODUCT,
						AppConst.VALUES_PRODUCT,
						false
				),
				new BatchPreparedStatementSetter() {
					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						ProductEntity c = list.get(arg1);
						
						arg0.setString		(1,  c.getCode());
						arg0.setString		(2,  c.getDescription());
						arg0.setInt			(3,  c.getStock());
						arg0.setBigDecimal	(4,  c.getPrice());
					}
				}
			   );
	}

	@Override
	public boolean delete(ProductEntity p) {
		return getJdbcTemplate().update(
				methodsSql.delete(
						AppConst.TABLE_PRODUCT,
						AppConst.ID_PRODUCT
				),
				p.getId()
				)>0;
	}

	@Override
	public List<ProductEntity> getAll() {
		return getJdbcTemplate().query(
				methodsSql.select(AppConst.TABLE_PRODUCT), 
				new ProductRowMapper()
				);
	}

	@Override
	public ProductEntity getItemById(Integer id) {
		List<ProductEntity> products = getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_PRODUCT,
						AppConst.ID_PRODUCT
						),
				new Object[] {id},
				new ProductRowMapper()
				);
		if(products.isEmpty()) return null;
		return products.get(0);
	}

	@Override
	public int getCount() {
		return getJdbcTemplate().queryForObject(
				methodsSql.count(AppConst.TABLE_PRODUCT), 
				Integer.class);
	}

	@Override
	public boolean update(ProductEntity p) {
		return getJdbcTemplate().update(
				methodsSql.update(
						AppConst.TABLE_PRODUCT,
						AppConst.VALUES_PRODUCT,
						AppConst.ID_PRODUCT
					),
					new Object[] 
					{
						p.getCode(),
						p.getDescription(),
						p.getStock(),
						p.getPrice(),
						p.getId(),//al final el id de la tabla
					})>0;
	}

	@Override
	public ProductEntity getItemByCode(String code) {
		List<ProductEntity> products = getJdbcTemplate().query(
				methodsSql.selectById(
						AppConst.TABLE_PRODUCT,
						"code"
						),
				new Object[] {code},
				new ProductRowMapper()
				);
		if(products.isEmpty()) return null;
		return products.get(0);
	}

	@Override
	public Integer insertAndReturnKey(ProductEntity c) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(
		    new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(java.sql.Connection con) throws SQLException {
					
					PreparedStatement arg0 = con.prepareStatement(
							methodsSql.insert(
									AppConst.TABLE_PRODUCT,
									AppConst.VALUES_PRODUCT,
									true)
							, 
							new String[] {"id"}
							);

					arg0.setString	(1,  c.getCode());
					arg0.setString	(2,  c.getDescription());
					arg0.setInt		(3,  c.getStock());
					arg0.setBigDecimal	(4,  c.getPrice());
			        return arg0;
				}
		    },
		    keyHolder);
		return (Integer) keyHolder.getKey();
	}

	@Transactional
	@Override
	public boolean updateList(List<ProductEntity> list) {
		return getJdbcTemplate().batchUpdate(
				AppConst.UPDATE_PRODUCT,
				new BatchPreparedStatementSetter() {
					@Override
					public int getBatchSize() {
						return list.size();
					}

					@Override
					public void setValues(PreparedStatement arg0, int arg1) throws SQLException {
						ProductEntity c = list.get(arg1);
						arg0.setInt			(1,  c.getStock());
						arg0.setInt			(2, c.getId());//id al final
					}
				}
			   )!=null;
	}

}
