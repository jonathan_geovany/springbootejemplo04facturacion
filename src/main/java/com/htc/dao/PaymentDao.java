package com.htc.dao;

import java.util.List;

import com.htc.model.PaymentEntity;


public interface PaymentDao {
	public boolean insert(PaymentEntity p);
	public Integer insertAndReturnKey(PaymentEntity p);
	public boolean update(PaymentEntity p);
	public int[] insertList(List<PaymentEntity> list);
	public boolean delete(PaymentEntity p);
	public List<PaymentEntity> getAll();
	public PaymentEntity getItemById(Integer id);
	public int getCount();
}
