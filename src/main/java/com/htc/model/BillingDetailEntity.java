package com.htc.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class BillingDetailEntity implements Serializable {

	/**
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer idBill;
	private Integer idProduct;
	private BigDecimal	price;
	private Integer quantity;
	private BigDecimal	total;
	private ProductEntity productEntity;
	
	public BillingDetailEntity() {
		super();
	}
	
	
	
	

	public BillingDetailEntity(Integer idProduct, Integer quantity) {
		super();
		this.idProduct = idProduct;
		this.quantity = quantity;
	}





	public BillingDetailEntity(Integer idProduct, BigDecimal price, Integer quantity, BigDecimal total) {
		super();
		this.idProduct = idProduct;
		this.price = price;
		this.quantity = quantity;
		this.total = total;
	}





	public BillingDetailEntity(Integer idBill, Integer idProduct, BigDecimal price, Integer quantity, BigDecimal total) {
		super();
		this.idBill = idBill;
		this.idProduct = idProduct;
		this.price = price;
		this.quantity = quantity;
		this.total = total;
	}


	
	

	public ProductEntity getProduct() {
		return productEntity;
	}





	public void setProduct(ProductEntity productEntity) {
		this.productEntity = productEntity;
	}





	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdBill() {
		return idBill;
	}

	public void setIdBill(Integer idBill) {
		this.idBill = idBill;
	}


	public Integer getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}



	public BigDecimal getPrice() {
		return price;
	}



	public void setPrice(BigDecimal price) {
		this.price = price;
	}





	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillingDetailEntity [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (idBill != null) {
			builder.append("idBill=");
			builder.append(idBill);
			builder.append(", ");
		}
		if (idProduct != null) {
			builder.append("idProduct=");
			builder.append(idProduct);
			builder.append(", ");
		}
		if (price != null) {
			builder.append("price=");
			builder.append(price);
			builder.append(", ");
		}
		if (quantity != null) {
			builder.append("quantity=");
			builder.append(quantity);
			builder.append(", ");
		}
		if (total != null) {
			builder.append("total=");
			builder.append(total);
			builder.append(", ");
		}
		if (productEntity != null) {
			builder.append("productEntity=");
			builder.append(productEntity);
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
