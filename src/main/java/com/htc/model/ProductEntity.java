package com.htc.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String code;
	private String description;
	private Integer stock;
	private BigDecimal price;
	public ProductEntity() {
		super();
	}
	
	


	public ProductEntity(String code, String description, Integer stock, BigDecimal price) {
		super();
		this.code = code;
		this.description = description;
		this.stock = stock;
		this.price = price;
	}




	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}




	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductEntity [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", description=");
		builder.append(description);
		builder.append(", stock=");
		builder.append(stock);
		builder.append(", price=");
		builder.append(price);
		builder.append("]");
		return builder.toString();
	}




}
