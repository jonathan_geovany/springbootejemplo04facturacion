package com.htc.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class EmployeeEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer idBusiness;
	private String names;
	private String dui;
	private String position;
	private BigDecimal  salary;
	private String phone;
	private String address;
	private String email;
	
	public EmployeeEntity() {
		super();
	}
	
	public EmployeeEntity(Integer idBusiness, String names, String dui, String position, BigDecimal salary, String phone,
			String address, String email) {
		super();
		this.idBusiness = idBusiness;
		this.names = names;
		this.dui = dui;
		this.position = position;
		this.salary = salary;
		this.phone = phone;
		this.address = address;
		this.email = email;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdBusiness() {
		return idBusiness;
	}


	public void setIdBusiness(Integer idBusiness) {
		this.idBusiness = idBusiness;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeEntity [id=");
		builder.append(id);
		builder.append(", idBusiness=");
		builder.append(idBusiness);
		builder.append(", names=");
		builder.append(names);
		builder.append(", dui=");
		builder.append(dui);
		builder.append(", position=");
		builder.append(position);
		builder.append(", salary=");
		builder.append(salary);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", address=");
		builder.append(address);
		builder.append(", email=");
		builder.append(email);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
