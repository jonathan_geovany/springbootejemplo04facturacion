package com.htc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public class BillEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Integer idBusiness;
	private Integer idEmployee;
	private Integer idClient;
	private Integer idPayment;
	private Integer idStatus;
	private String 	observations;
	private String	register;
	private Timestamp	emitted;
	private BigDecimal 	tax;
	private BigDecimal 	taxAmount;
	private BigDecimal 	subtotal;
	private BigDecimal 	total;
	
	private List<BillingDetailEntity> details;
	
	
	public BillEntity() {
		super();
	}
	
	public BillEntity(Integer idBusiness, Integer idEmployee, Integer idClient, Integer idPayment, Integer idStatus,
			String observations, String register, Timestamp emitted, List<BillingDetailEntity> details) {
		super();
		this.idBusiness = idBusiness;
		this.idEmployee = idEmployee;
		this.idClient = idClient;
		this.idPayment = idPayment;
		this.idStatus = idStatus;
		this.observations = observations;
		this.register = register;
		this.emitted = emitted;
		this.details = details;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdBusiness() {
		return idBusiness;
	}


	public void setIdBusiness(Integer idBusiness) {
		this.idBusiness = idBusiness;
	}


	public Integer getIdEmployee() {
		return idEmployee;
	}


	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public Integer getIdPayment() {
		return idPayment;
	}


	public void setIdPayment(Integer idPayment) {
		this.idPayment = idPayment;
	}

	public String getRegister() {
		return register;
	}

	public void setRegister(String register) {
		this.register = register;
	}

	public Timestamp getEmitted() {
		return emitted;
	}
	
	public void setEmitted(Timestamp emitted) {
		this.emitted = emitted;
	}
	
	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}


	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}


	public BigDecimal getTotal() {
		return total;
	}


	public void setTotal(BigDecimal total) {
		this.total = total;
	}


	public List<BillingDetailEntity> getDetails() {
		return details;
	}


	public void setDetails(List<BillingDetailEntity> details) {
		this.details = details;
	}


	public String getObservations() {
		return observations;
	}


	public void setObservations(String observations) {
		this.observations = observations;
	}

	public Integer getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BillEntity [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (idBusiness != null) {
			builder.append("idBusiness=");
			builder.append(idBusiness);
			builder.append(", ");
		}
		if (idEmployee != null) {
			builder.append("idEmployee=");
			builder.append(idEmployee);
			builder.append(", ");
		}
		if (idClient != null) {
			builder.append("idClient=");
			builder.append(idClient);
			builder.append(", ");
		}
		if (idPayment != null) {
			builder.append("idPayment=");
			builder.append(idPayment);
			builder.append(", ");
		}
		if (idStatus != null) {
			builder.append("idStatus=");
			builder.append(idStatus);
			builder.append(", ");
		}
		if (observations != null) {
			builder.append("observations=");
			builder.append(observations);
			builder.append(", ");
		}
		if (register != null) {
			builder.append("register=");
			builder.append(register);
			builder.append(", ");
		}
		if (emitted != null) {
			builder.append("emitted=");
			builder.append(emitted);
			builder.append(", ");
		}
		if (tax != null) {
			builder.append("tax=");
			builder.append(tax);
			builder.append(", ");
		}
		if (taxAmount != null) {
			builder.append("taxAmount=");
			builder.append(taxAmount);
			builder.append(", ");
		}
		if (subtotal != null) {
			builder.append("subtotal=");
			builder.append(subtotal);
			builder.append(", ");
		}
		if (total != null) {
			builder.append("total=");
			builder.append(total);
			builder.append(", ");
		}
		if (details != null) {
			builder.append("details=");
			builder.append(details);
		}
		builder.append("]");
		return builder.toString();
	}


}
