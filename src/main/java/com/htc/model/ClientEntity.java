package com.htc.model;

import java.io.Serializable;

public class ClientEntity implements Serializable {

	/**
	 * 	
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String names;
	private String nit;
	private String dui;
	private String phone;
	private String address;
	
	public ClientEntity() {
		super();
	}
	
	public ClientEntity(String names, String nit, String dui, String phone, String address) {
		super();
		this.names = names;
		this.nit = nit;
		this.dui = dui;
		this.phone = phone;
		this.address = address;
	}

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}


	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	
	public String getDui() {
		return dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ClientEntity [id=");
		builder.append(id);
		builder.append(", names=");
		builder.append(names);
		builder.append(", nit=");
		builder.append(nit);
		builder.append(", dui=");
		builder.append(dui);
		builder.append(", phone=");
		builder.append(phone);
		builder.append(", address=");
		builder.append(address);
		builder.append("]");
		return builder.toString();
	}

	

}
