package com.htc.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class BusinessEntity implements Serializable{

	/**
	 *
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String tradename;
	private String businessName;
	private String sector;
	private String nit;
	private String nrc;
	private String country;
	private String city;
	private String address;
	private String phone1;
	private String phone2;
	private String email;
	private BigDecimal tax;
	
	public BusinessEntity() {
		super();
	}
	
	
	public BusinessEntity(String tradename, String businessName, String sector, String nit, String nrc, String country,
			String city, String address, String phone1, String phone2, String email, BigDecimal tax) {
		super();
		this.tradename = tradename;
		this.businessName = businessName;
		this.sector = sector;
		this.nit = nit;
		this.nrc = nrc;
		this.country = country;
		this.city = city;
		this.address = address;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.email = email;
		this.tax = tax;
	}







	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public BigDecimal getTax() {
		return tax;
	}


	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}



	public String getTradename() {
		return tradename;
	}

	public void setTradename(String tradename) {
		this.tradename = tradename;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getNrc() {
		return nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BusinessEntity [id=");
		builder.append(id);
		builder.append(", tradename=");
		builder.append(tradename);
		builder.append(", businessName=");
		builder.append(businessName);
		builder.append(", sector=");
		builder.append(sector);
		builder.append(", nit=");
		builder.append(nit);
		builder.append(", nrc=");
		builder.append(nrc);
		builder.append(", country=");
		builder.append(country);
		builder.append(", city=");
		builder.append(city);
		builder.append(", address=");
		builder.append(address);
		builder.append(", phone1=");
		builder.append(phone1);
		builder.append(", phone2=");
		builder.append(phone2);
		builder.append(", email=");
		builder.append(email);
		builder.append(", tax=");
		builder.append(tax);
		builder.append("]");
		return builder.toString();
	}
	
	
}
