package com.htc.model;

import java.io.Serializable;

public class PaymentEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String description;
	
	public PaymentEntity() {
		super();
	}
	
	
	
	public PaymentEntity(String description) {
		super();
		this.description = description;
	}



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaymentEntity [id=");
		builder.append(id);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}


	
	
}
